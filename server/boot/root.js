
module.exports = function(server) {
  'use strict';
  var datasource = require('../../server/datasources.json');

  // Install a `/` route that returns server status

  var router = server.loopback.Router();
  var User = server.models.PGQ_User;
  var Token = server.models.AccessToken;
  router.get('/', server.loopback.status());

  //verified
  router.get('/verified', function(req, res) {
    res.render('verified');
  });

  //send an email with instructions to reset an existing user's password
  router.post('/request-password-reset', function(req, res, next) {
    User.resetPassword({
      email: req.body.email
    }, function(err) {
      if (err) {return res.status(401).send(err);}
      res.render('response', {
        title: 'Password reset requested',
        content: 'Check your email for further instructions',
        redirectTo: '',
        redirectToLinkText: ''
      });
    });
  });

  //show password reset form
  router.get('/reset-password', function(req, res, next) {
    //console.log(req.query.accessToken);
    //console.log(req.query.access_token);
    if (!req.accessToken) {
      return res.sendStatus(401);
    }
    res.render('password-reset', {
      accessToken: req.accessToken.id
      //accessToken: req.query.access_token
    });
  });

  //reset the user's pasword
  router.post('/reset-password', function(req, res, next) {
    if (!req.accessToken){
       return res.sendStatus(401);
    }
    //console.log(req.accessToken.userId);

    //verify passwords match
    if (!req.body.password ||
        !req.body.confirmation ||
        req.body.password !== req.body.confirmation) {
      return res.sendStatus(400, new Error('Passwords do not match'));
    }

    User.findById(req.accessToken.userId, function(err, user) {
      if (err) {
        return res.sendStatus(404);
      }
      user
      .updateAttribute(
        'password', req.body.password, (err, userUpdate) => {
          if (err){
            return res.sendStatus(404);
          }

          var html =`<link rel="stylesheet" href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
                      <link rel="stylesheet" href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css' integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
                      <style>
                       body{
                         background-color: #009688;
                      }
                      .formGatey{
                         background-color: rgba(255,255,255,0.8);
                         padding: 10px;
                         margin-top: 50px;
                         width: auto;
                      }
                      .resetPass{
                         margin: 10px;
                      }
                      .inputGatey{
                         margin-bottom: 20px;
                      }
                      .imgLogo{
                         width: 200px;
                         height: auto;
                         margin-top: 50px;
                      }
                      </style>
                      <section>
                       <form action="/reset-password?access_token=<%= accessToken %>" method="post" >
                         <center>
                           <img class="imgLogo" src="logo.png" alt="">
                         </center>
                         <div class="container">
                           <fieldset class="formGatey">
                             <div class="row">
                               <div class="col-md-12 text-center">
                                 <legend><h3>Your password has been reset successfully</h3></legend>
                               </div>
                             </div>
                           </fieldset>
                         </div>
                       </form>
                      </section>
                      <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js'></script>
                      <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js' integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
                      `;

        Token.destroyAll({id : req.accessToken.id}, function(err, result){
          if (err) {throw err;}
          console.log(result);
        });
        //Envio de correo
        User.app.models.Email.send({
          to: user.email,
          from: datasource.emailDs.transports[0].auth.user,
          subject: 'Password reset successfully',
          text: '',
          html: html
        }, (err, mail) => {
          if (err) {
            return console.log('> error sending password reset email', mail);
          }else {
            //console.log('> password reset processed successfully');
            res.render('response', {
              title: 'Password reset success',
              content: 'Your password has been reset successfully',
              redirectTo: '#',
              redirectToLinkText: ''
            });
          }
        });
        //Envio de correo
      });
    });
  });

  server.use(router);

};

'use strict';
var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

//Configuración de vistas para resetedar el password
  var bodyParser = require('body-parser');
  var path = require('path');

  // configure view handler
  app.set('view engine', 'ejs');
  app.set('views', path.join(__dirname, 'views'));

  // configure body parser
  app.use(bodyParser.urlencoded({extended: true}));

  //app.use(loopback.token());
  app.use(loopback.token({ model: app.models.accessToken }));

//Configuración de vistas para resetedar el password
/*
//Función recursiva para validar expiración de codeQr
var expireQRCodePromise = function(userId) {
  return new Promise(function (resolve, reject){
  console.log('Recursión activa');
  function expireQRCode(userId){
    console.log(userId);
    var Qrcode = app.models.PGQ_QRCode;
    Qrcode.reqQrCode(userId,function(err,result){
      if (err){
        console.log('Error: '+err);
        //return err;
        //return Promise.reject(err);
        return reject(err);
      }else{
        if (result.length>0){
          if (result.idCodeQr==='' && result.status===false && result.date===''){
              console.log('Result: '+result);
              return resolve(result);
              //return (result);
          }else{
            console.log('Recursion en proceso');
            return expireQRCode(userId);
          }
        }
      }
    });
 }
});
};
//Función recursiva para validar expiración de codeQr
*/
app.start = function() {
  // start the web server
  var server = app.listen(function() {
    app.emit('started', server);
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
  return server;
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) {throw err;}

  // start the server if `$ node server.js`
  if (require.main === module){
    //app.start();

    //Socketio
    app.io = require('socket.io')(app.start());
    require('socketio-auth')(app.io, {
      authenticate: function (socket, value, callback) {
        console.log(value);
      var AccessToken = app.models.AccessToken;
      //get credentials sent by the client
      var token = AccessToken.find({
        where:{
                and: [{ userId: value.userId }, { id: value.id }]
        }
      },
      function(err, tokenDetail){
        if (err) {throw err;}
        console.log(tokenDetail);
        //socket.emit('objectToken',tokenDetail);
        if(tokenDetail.length){
          callback(null, true);
        }else {
          callback(null, false);
        }
      }); //find function..
      } //authenticate function..
    });

    //Conexión y manejo de eventos con socketio
    app.io.on('connection', function(socket){
      console.log('a user connected');
      socket.on('ouputQRCode',function(userJson){
        console.log('ouputQRCode');
        var Qrcode = app.models.PGQ_QRCode;
        Qrcode.reqQrCode(userJson.userId,function(err,result){
          console.log('Id User: '+userJson.userId);
          if (err){
            console.log('QRCode no lo encontro '+err);
            socket.emit('QR',err);
          }else{
            console.log(result);
            console.log('QRCode lo encontro');
            socket.emit('QR',result);
            //socket.emit('QRExpire',expireQRCode(userJson.userId));
            // expireQRCodePromise(userJson.userId).then(json => {
            //   console.log('Qr Expiro');
            //   socket.emit('QRExpire',json);
            // });
          }
        });
      });
    });
    //Conexión y manejo de eventos con socketio

  }
});

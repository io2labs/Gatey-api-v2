'use strict';

//Importando librerias de seguridad
import {
	createCodeEncrypt,
	codeDecrypt
} from './security';

let app = require('../server.js');
const datasource = require('../datasources.json');
let ObjectID = require('mongodb').ObjectID;

//Promise que se encarga de generar codigo aleatorio
export function getCodeRandom(
): Promise<string> {
  return new Promise((resolve: any, reject: any) => {
		let code = Math.round(Math.random()*99999);
		resolve(code);
  });
}

//Promise que se encarga de obtener los datos de la invitación
export function getInvited(
	code: string,
	email: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		let Invited: any = app.models.PGQ_Invited;

		Invited.find({
			where:{
				check: true,
				email: email,
				code: code
			}
		},(err: any, invitedResult: any) => {
				if (err) { reject(err); }
				console.log('getInvited => Object Invited : ',invitedResult[0]);
				console.log('Count : ',invitedResult.length);
				resolve(invitedResult);
		});
  });
}

//Promise que se encarga de verificar el usuario
//en la comunidad y verificar que usuarios lo tienen como contacto
export function validUserCommunityContact(
	idUserContact: string,
	idCommunity: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		let Members: any = app.models.PGQ_Members;
		idUserContact = new ObjectID(idUserContact);
    idCommunity = new ObjectID(idCommunity);

		Members.find({
			where: {
				fkUserId: idUserContact,
				status: true,
				check: false
			},
			include: {
				relation: 'groupMembers',
				scope: {
					include: {
						relation: 'membersCommunity',
						scope: {
							where: {
								fkCommunityId: idCommunity,
								status: true
							}
						}
					}
				}
			}
		}, (err: any, members: any) => {
				if (err) { reject(err); }
				console.log('members ............. : ',members);
					//Armado de array para proceder a a borrar o no el membersCommunity
					let datos: Array<any> = [];
					for (let i = 0; i < members.length; i++) {
						console.log(members[i].groupMembers().membersCommunity());
						if(members[i].groupMembers().membersCommunity()) {
								console.log('***********hola**************');
								let object: any = {};
								object.fkMembersId = new ObjectID(members[i].id);
								datos.push(object);
						}
					}
					if(datos.length>0) {
						resolve(datos);
					} else {
						resolve([]);
					}
		});
  });
}

//Promise que se encarga de validar los habitantes
export function validationHabitant(
	idMembersCommunity: string,
	idUser: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		getUserHabitant(idMembersCommunity,idUser).then(objectHabitant => {
			console.log('Count validation Habitant : ',objectHabitant.length);
			if(objectHabitant.length===0 || (objectHabitant.length===1 && objectHabitant[0].status===false && objectHabitant[0].locked===false)) {
				resolve(true);
			} else if(objectHabitant.length===1 && objectHabitant[0].status===true && objectHabitant[0].locked===false) {
					console.log('----entro 1-----');
					console.log('Status validation Habitant : ',objectHabitant[0].status);
					let error = 'Contact already exists as habitant.';
					let errQrcode: any = new Error(error);
					errQrcode.statusCode = 400;
					reject(errQrcode);
			} else if(objectHabitant.length===1 && objectHabitant[0].status===true && objectHabitant[0].locked===true) {
				console.log('----entro 2-----');
					let error = 'Contact already exists as habitant, but is blocked.';
					let errQrcode: any = new Error(error);
					errQrcode.statusCode = 400;
					reject(errQrcode);
			}
		});
  });
}

//Promise que se encarga de validar los visitantes
export function validationVisitant(
	idMembersCommunity: string,
	idCommunity: string,
	idUser: string,
	type: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
					getGroupUser(idMembersCommunity,'default').then(idGroup => {
						getMembers(idGroup,idUser,true,false,true).then(members => {
							if(members.length>0) {
								let error = 'Contact already exists.';
								let errQrcode: any = new Error(error);
								errQrcode.statusCode = 400;
								reject(errQrcode);
							} else {
								resolve(true);
							}
						},(err: any) => {
							reject(err);
						});
			}, (err: any) => {
				reject(err);
			});
	});
}

//Promise que se encarga de realizar validaciones a nivel de contactos
export function validationTypeContact(
	type: string,
	idUser: string,
	idCommunity: string,
	idMembersCommunity: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
	validationVisitant(idMembersCommunity,idCommunity,idUser,type).then(validVisitant => {
		if(validVisitant) {
			resolve(true);
		}
	},(err: any) => {
		reject(err);
	});
});
}

//Promise que se encarga de obtener el roleCommunity
export function getRoleCommunity(
	name: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		let roleCommunity: any = app.models.PGQ_RoleCommunity;
		roleCommunity.find({
			where: {
				status: true,
				name: name
			}
		}, (err: any, roleResult: any) => {
				if (err) { throw err; }

				console.log('getRoleCommunity => roleResult : ',roleResult);

				if (roleResult.length > 0) {
					resolve(roleResult);
				} else {
					resolve(false);
				}
		});
	});
}

//Promise que se encarga de chequear si el usuario existe
export function getCheckUser(
	email: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		let User: any = app.models.PGQ_User;
		User.find({
			where:{
				email: email
			}
		},(err: any, userResult: any) => {
				if (err) {reject(err);}
				console.log('getCheckUser => Object User : ',userResult);
				console.log('Count : ',userResult.length);
				resolve(userResult);
		});
  });
}

//Promise que se encarga de enviar correo electronico
export function sendEmail(
	email: string,
	html: string,
	subject: string,
	text: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {

		app.models.Email.send({
				to: email,
				from: datasource.emailDs.transports[0].auth.user,
				subject: subject,
				text: text,
				html: html
		}, (err, mail) => {
				if (err) {
						console.log('Error de envio');
						reject(err);
				} else {
					console.log('Se envio el correo');
					console.log(mail);
           resolve(true);
				}
		});
  });
}

//Promise que genera una invitación
export function createInvitation(
	type: string,
	email: string,
	membersCommunity: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		console.log('Entro');
		//La idea de esta función (getRoleCommunity) es para utilizar el rolCommunity de forma dinamica
		getRoleCommunity(type).then(roleResult => {
			console.log('Role Community : ',roleResult);
			if (roleResult.length > 0) {
				getCodeRandom().then(code => {
						let Invited: any = app.models.PGQ_Invited;
						Invited.create({
							email: email,
							code: code,
							created: Date(),
							type: type,
							fkMembersCommunityId: membersCommunity
						}, (err: any, invited: any) => {
							if (err) { throw err; }
							if (invited) {
									resolve(invited);
							} else {
									let error = 'The invitation was not sent.';
									let errQrcode: any = new Error(error);
									errQrcode.statusCode = 400;
									reject(errQrcode);
							}
						});
				});
			} else {
					let error = 'Role does not exist.';
					let errQrcode: any = new Error(error);
					errQrcode.statusCode = 400;
					reject(errQrcode);
			}
		});
  });
}

//Promise que se encarga de revisar si la invitación existe
export function checkInvitation(
	type: string,
	email: string,
	fkMembersCommunityId: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		fkMembersCommunityId = new ObjectID(fkMembersCommunityId);
		let Invitation: any = app.models.PGQ_Invited;
		Invitation.find({
			where:{
				email: email,
				type: type,
				fkMembersCommunityId: fkMembersCommunityId,
				check: true
			}
		},(err: any, invitationResult: any) => {
				if (err) {reject(err);}
				console.log('Object Invitation Promise : ',invitationResult);
				console.log('Count : ',invitationResult.length);
				resolve(invitationResult);
		});
  });
}

//Promise para sincronia de Buffer de image codeQR
export function promiseQR(
	crypted: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    //Creación y generación de imagen Qr
    let fs = require('fs');
    let qrCode = require('qr-image');
    var url = `${__dirname}/../../image-qr/${crypted}.png`;
    console.log(url);
    let generatCode = qrCode.image(crypted,{type: 'png'});
    let output = fs.createWriteStream(url.toString());
    let png = generatCode.pipe(output);
    //Creación y generación de imagen Qr
    if (png){
      resolve(png);
    }else{
      reject(false);
    }
  });
}

//Promesa que desactiva los codeQR
export function deactivateCodeQR(
	id: any,
	type: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    //Declaración de varibles
		let QRCode: any = app.models.PGQ_QRCode;

		if(type==='M') {
			QRCode.updateAll({
					status: true,
					fkMembersId: id
			},{
					status: false
			},(err,deleteCodeQR)=> {
					if (err) {reject(err);}

					if(deleteCodeQR.count>0) {
						resolve(true);
					} else {
						resolve(false);
					}
			});
		} else if(type==='H') {
				QRCode.updateAll({
					status: true,
					fkHabitantId: id
				},{
					status: false
				},(err,deleteCodeQR)=> {
					if (err) {reject(err);}

					if(deleteCodeQR.count>0) {
						resolve(true);
					} else {
						resolve(false);
					}
				});
		}
  });
}

//Promesa que se encarga de generar notificaciones de cualquier tipo
export function notificationContact(
	idUserSend: any,
	idUserRecipient: any,
	type: any,
	typeDetaild: any,
	detailObject: any,
	communityObject: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    //Declaración de varibles
    var Notification = app.models.PGQ_Notification;
    idUserSend = new ObjectID(idUserSend);
    idUserRecipient = new ObjectID(idUserRecipient);

    Notification
      .create({
        created: Date(),
        detail: detailObject,
        community: communityObject,
        type: type,
				typeDetaild: typeDetaild,
        fkUserId: idUserRecipient,
        fkUserIdSend:idUserSend
      },(err: any, result: any ) => {
        if (err) {reject(err);}
        resolve(result);
    });
  });
}

//Promesa que se encarga de revisar la existencia de contacto, para proceder a crearlo o activarlo
export function updateOrCreateContact(
	fkUserId: any,
	fkGroupUserId: any,
	typeContact: number,
	typeRole = 'Resident'
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    //Declaración de varibles
    let Members: any  = app.models.PGQ_Members;
		let check: boolean = false;
		let status: boolean = false;
    fkUserId = new ObjectID(fkUserId);
    fkGroupUserId = new ObjectID(fkGroupUserId);

		console.log('Crear o Actualizar Members -> Contactos');
		console.log('fkUserId : ', fkUserId);
		console.log('fkGroupUserId : ', fkGroupUserId);

		//Activa o bloque los contactos
		if(typeContact===1){
			check = true;
			status = false;
		} else {
			check = false;
			status = true;
		}
		//Activa o bloque los contactos
    Members.find({
			where: {
				status: false,
				check: false,
				fkUserId: fkUserId,
				fkGroupUserId: fkGroupUserId,
				type: typeRole
			}
		}, (err: any, members: any) => {
			if (err) { reject(err); }

			console.log('Nueva Promesa => Members: ',members);

			if (members.length>0) {
				Members.updateAll({
					status: false,
					check: false,
					fkUserId: fkUserId,
					fkGroupUserId: fkGroupUserId,
					type: typeRole
				},{
					check: check,
					status: status,
					description: {name: 'default',status: false}
				},(err, membersUpdate) => {
					if (err) { reject(err); }

					if(membersUpdate.count>0){
						resolve(members[0]);
					} else {
						reject(err);
					}
				});
			} else {
					Members.create({
						fkGroupUserId: fkGroupUserId,
						fkUserId: fkUserId,
						created: Date(),
						type: typeRole,
						status: status,
						check: check
					},(err, membersCreate) => {
							if (err) {throw err;}
							if(membersCreate) {
								resolve(membersCreate);
							} else {
								reject(err);
							}
					});
			}
		});
  });
}

//Promesa para obtener datos del usuario
export function getUser(
	idUser: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    var User = app.models.PGQ_User;
    User
      .find({
        where: {
          _id: idUser
        }
      }, (err: any, result: any) => {
        if (err) {reject(err);}
        resolve(result);
      });
  });
}

//Promesa para obtener datos del usuario habitante
export function getUserHabitant(
	idMembersCommunity: any,
	fkUserId: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    var Habitant = app.models.PGQ_Habitant;
    Habitant
      .find({
        where: {
					fkUserId: fkUserId,
          fkMembersCommunityId: idMembersCommunity
        }
      }, (err: any, habitant: any) => {
        if (err) {reject(err);}
        resolve(habitant);
      });
  });
}

//Promesa para validar codeQR por comunidad y usuario
export function validCodeQr(
	arrayMembersId: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		let CodeQr: any = app.models.PGQ_QRCode;

		CodeQr.find({
			where: {
				or: arrayMembersId,
				status: true
			}
		}, (err: any, codeQr: any) => {
				if (err) {reject(err);}
				console.log('codeQr : ',codeQr);
				if(codeQr.length>0) {
					reject(codeQr);
				} else {
					resolve(true);
				}
		});
  });
}

/* Estructura de parametro para
 creación del objecto del codeQr */
interface ICodeQr {
	code: string,
	deliveryDate: Date,
	timeExpire: Date,
	created: Date,
	status: boolean,
	fkMembersId: string
};

//Promesa que se encarga de crear el codeQr
export function createCodeQr(
	objectCodeQr: ICodeQr
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		let CodeQr: any = app.models.PGQ_QRCode;

		console.log('createCodeQr Promise : ',objectCodeQr);

		//Configurar deliveryDate, timeExpire, graceTime
		let deliveryDateExpire = new Date(objectCodeQr.deliveryDate);
		let graceTimeH = objectCodeQr.timeExpire.getHours();
		let graceTimeM = objectCodeQr.timeExpire.getMinutes();

		//let graceTimeH = objectCodeQr.timeExpire.getHours();
		//let graceTimeM = objectCodeQr.timeExpire.getMinutes();

		//Hours
		let getHours = deliveryDateExpire.getHours();
		let getHoursTotal = getHours + graceTimeH;

		//Minutes
		let getMinutes = deliveryDateExpire.getMinutes();
		let getMinutesTotal = getMinutes + graceTimeM;
		if (getMinutesTotal >= 60) {
				getHoursTotal = getHoursTotal + 1;
				getMinutesTotal = getMinutesTotal - 60;
		}

		//Seconds
		let getSeconds = deliveryDateExpire.getUTCSeconds();

		//Setear tiempo de expiración
		objectCodeQr.timeExpire.setHours(getHoursTotal);
		objectCodeQr.timeExpire.setMinutes(getMinutesTotal);
		objectCodeQr.timeExpire.setSeconds(getSeconds);
		let calcDay: number = getHoursTotal/24;
		console.log('calcDay : ',calcDay);
		console.log('objectCodeQr.deliveryDate.getDay() : ',objectCodeQr.deliveryDate.getDate());
		console.log('	objectCodeQr.timeExpire --Antes-- : ',objectCodeQr.timeExpire);
		if(getHoursTotal>=1) {
			console.log('hola');
			let day: number = Math.floor(calcDay);
			objectCodeQr.timeExpire.setDate(objectCodeQr.deliveryDate.getDate()+day);
		}
		//Configurar deliveryDate, timeExpire, graceTime
		console.log('	objectCodeQr.timeExpire --Despues-- : ',objectCodeQr.timeExpire);
		console.log('createCodeQr Promise 2 : ',objectCodeQr);

		//Crear codeQr
		CodeQr.create(
			objectCodeQr
		, (err: any, createCodeQr: any)=> {
			if (err) {reject(err);}

			if(createCodeQr) {
				createCodeEncrypt(createCodeQr.id).then(crypted => {
						promiseQR(crypted).then((png: any) => {
							if (png) {
									CodeQr.updateAll({
											status: true,
											_id: createCodeQr.id
									},{
											code: crypted
									}, (err: any, updateCodeQr: any) => {
											if (err) {reject (err);}
											if(updateCodeQr.count===1) {
												//Obtener grupo --- Necesario para las notificaciones ---
													let Members: any = app.models.PGQ_Members;
													Members.find({
														fields: ['fkGroupUserId','fkUserId','type'],
														where: {
															id: objectCodeQr.fkMembersId,
															or: [
																	{status: true},
																	{type: 'autoCodeQR'}
															],
														}
													},(err: any, resultId: any) => {
														if (err) {reject(err);}
														console.log('Members Group : ', resultId);
														if(resultId.length===1) {
															//Obtener codeQR completo con id encriptado
															CodeQr.find({
																where: {
																	id: createCodeQr.id,
																	status: true
																}
															}, (err: any, codeQrObject: any) => {
																if (err) {reject (err);}
																console.log('CodeQR Object : ',codeQrObject);
																if(codeQrObject.length===1 && resultId[0].type!='autoCodeQR') {
																	/*--------------Generar notificación----------------*/
																		getCommunity(resultId[0].fkGroupUserId).then(
																			communityObject => {
																				getUserId(resultId[0].fkGroupUserId).then(
																					userId => {
																						notificationContact(
																							userId,resultId[0].fkUserId,3,
																							'Send you a QRcode gatey',codeQrObject[0],
																							communityObject[0]
																						).then(result => {
																							if(result){
																								resolve(true);
																							}else{
																							resolve(false);
																							}
																					}, (err: any) => {
																						reject(err);
																					});
																			}, (err: any) => {
																					reject(err);
																			});
																		}, (err: any) => {
																				reject(err);
																		});
																	/*--------------Generar notificación----------------*/
																} else {
																		resolve(true);
																}
															});
															//Obtener codeQR completo con id encriptado
														}
													});
												//Obtener grupo --- Necesario para las notificaciones ---
											}
									});
							}
					}, err => {
						console.log(err);
						reject(err);
					});
				});
				//resolve(createCodeQr);
			} else {
				let error = 'Codeqr could not be created.';
				let errQrcode: any = new Error(error);
				errQrcode.statusCode = 400;
				reject(errQrcode);
			}
	});
});
}

//Promesa que se encarga de validar codeQR por usuario y comunidad
export function validUserComunityCodeQr(
	idUserContact: any,
	idCommunity: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		let Members: any = app.models.PGQ_Members;
		idUserContact = new ObjectID(idUserContact);
    idCommunity = new ObjectID(idCommunity);

		Members.find({
			where: {
				fkUserId: idUserContact,
				status: true,
				check: false
			},
			include: {
				relation: 'groupMembers',
				scope: {
					include: {
						relation: 'membersCommunity',
						scope: {
							where: {
								fkCommunityId: idCommunity,
								status: true
							}
						}
					}
				}
			}
		}, (err: any, members: any) => {
				if (err) { reject(err); }

				if(members.length>0) {
					console.log('members : ',members);
					//Armado de array para revisar codeQr activos
					let datos: Array<any> = [];
					for (var i = 0; i < members.length; i++) {
						console.log(members[i].groupMembers().membersCommunity());
						if(members[i].groupMembers().membersCommunity()) {
								console.log('hola');
								let object: any = {};
								object.fkMembersId = new ObjectID(members[i].id);
								datos.push(object);
						}
					}
					console.log('datos : ',datos);
					validCodeQr(datos).then(codeQr => {
						if(codeQr) {
							console.log('No existe ningun codeQr Activo');
							resolve(true);
						}
					}, (existCodeQr: any) => {
						//Fecha Actual
						let fechaActual = new Date();

						//Fecha del codeQr
						let fechaCodeQr = existCodeQr[0].deliveryDate;

						//Fecha Expiración codeQr
						let fechaExpCodeQr = existCodeQr[0].timeExpire;

						// asignar el valor de las unidades en milisegundos
						let msecPerMinute = 1000 * 60;
						let msecPerHour = msecPerMinute * 60;
						let msecPerDay = msecPerHour * 24;

						// asignar la fecha en milisegundos
						let dateIniMsec = fechaCodeQr.getTime();
						let dateExpMsec = fechaExpCodeQr.getTime();
						let dateActualMsec = fechaActual.getTime();

						// Obtener la diferencia en milisegundos del codeQr
						let intervalCodeQr = (dateIniMsec - dateExpMsec)*(-1);
						let intervalActual = (dateIniMsec - dateActualMsec)*(-1);
						let intervalActive = dateIniMsec - dateActualMsec;

						if (intervalActive > 0) {
								let errText = 'QR code generated awaiting activation.';
								let errQrcode: any = new Error(errText);
								errQrcode.statusCode = 400;
								reject(errQrcode);
						} else {
								if ((intervalCodeQr > intervalActual) &&
												(existCodeQr[0].status === true && existCodeQr[0].statusIn === false &&
												existCodeQr[0].statusOut === false)) {
										let textError : string = 'Active QR code for this contact in the community.';
										let errQrcode: any = new Error(textError);
										errQrcode.statusCode = 400;
										reject(errQrcode);
								} else if ((intervalCodeQr > intervalActual) &&
												(existCodeQr[0].status === true && existCodeQr[0].statusIn === true &&
												existCodeQr[0].statusOut === false)) {
										let textError : string = 'Active QR code for this contact in the community. Check the entrance.';
										let errQrcode: any = new Error(textError);
										errQrcode.statusCode = 400;
										reject(errQrcode);
								}
								 else {
										if (existCodeQr[0].statusIn === false &&
												existCodeQr[0].statusOut===false) {
											//Proceso de desactivación del codeQR
											let QRCode: any = app.models.PGQ_QRCode;

											QRCode.updateAll({
													status: true,
													id: existCodeQr[0].id,
													statusIn: false,
													statusOut: false
											},{
													status: false
											}, (err: any, result: any) => {
													if (err) {reject(err);}
													if (result.count>0) {
														console.log('Elimino codeQr');
														resolve(true);
													}
											});
											//Proceso de desactivación del codeQR
								}
						}
					}
				});
			} else {
				let error = 'User community does not exist.';
				let errQrcode: any = new Error(error);
				errQrcode.statusCode = 400;
				reject(errQrcode);
			}
		});
  });
}

//Promesa que se encarga de escanear el codeQr
export function scanCodeQr(
	idCodeQr: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
	let CodeQr: any = app.models.PGQ_QRCode;

	CodeQr.find({
		where: {
			status: true,
			id: idCodeQr
		}
	}, (err: any, existCodeQr: any) => {
			console.log('existCodeQr : ',existCodeQr);
			if (existCodeQr.length>0) {
				//Fecha Actual
				let fechaActual = new Date();

				//Fecha del codeQr
				let fechaCodeQr = existCodeQr[0].deliveryDate;

				//Fecha Expiración codeQr
				let fechaExpCodeQr = existCodeQr[0].timeExpire;

				// asignar el valor de las unidades en milisegundos
				let msecPerMinute = 1000 * 60;
				let msecPerHour = msecPerMinute * 60;
				let msecPerDay = msecPerHour * 24;

				// asignar la fecha en milisegundos
				let dateIniMsec = fechaCodeQr.getTime();
				let dateExpMsec = fechaExpCodeQr.getTime();
				let dateActualMsec = fechaActual.getTime();

				// Obtener la diferencia en milisegundos del codeQr
				let intervalCodeQr = (dateIniMsec - dateExpMsec)*(-1);
				let intervalActual = (dateIniMsec - dateActualMsec)*(-1);
				let intervalActive = dateIniMsec - dateActualMsec;

				if ((intervalCodeQr > intervalActual) &&
					(existCodeQr[0].status === true && existCodeQr[0].statusIn === false &&
						existCodeQr[0].statusOut === false)) {
					CodeQr.updateAll({
							status: true,
							id: idCodeQr,
							statusIn: false,
							statusOut: false
					},{
							statusIn: true
					},(err, result) => {
							if (err) { reject(err); }
							console.log('result 1 : ',result);
							if(result.count > 0) {
									resolve(true);
							} else {
									let error = 'Could not check entry.';
									let errQrcode: any = new Error(error);
									errQrcode.statusCode = 400;
									reject(errQrcode);
							}
					});
				} else if ((intervalCodeQr > intervalActual) &&
								(existCodeQr[0].status === true && existCodeQr[0].statusIn === true &&
								existCodeQr[0].statusOut === false)) {
						CodeQr.updateAll({
								status: true,
								id: idCodeQr,
								statusIn: true,
								statusOut: false
						},{
								statusOut: true,
								status: false
						},(err, result) => {
								if (err) {reject(err);};
								console.log('result 2 : ',result);
								if(result.count > 0) {
										resolve(true);
								} else {
										let error = 'Could not check entry.';
										let errQrcode: any = new Error(error);
										errQrcode.statusCode = 400;
										reject(errQrcode);
								}
						});
				} else {
						let error = 'No information found.';
						let errQrcode: any = new Error(error);
						errQrcode.statusCode = 400;
						reject(errQrcode);
				}
			} else {
					let error = 'Codeqr not found.';
					let errQrcode: any = new Error(error);
					errQrcode.statusCode = 400;
					reject(errQrcode);
			}
		});
	});
}

//Promesa para obtener el membersCOmmunity
export function getMembersCommunity(
	idMembersCommunity = '',
	band = false,
	idUser = '',
	idCommunity = ''
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    var MembersCommunity = app.models.PGQ_MembersCommunity;
		if (band) {
			MembersCommunity
			.find({
				where: {
					fkCommunityId: idCommunity,
					fkUserId: idUser,
					status: true
				}
			}, (err: any, membersCommunity: any) => {
				if (err) {reject(err);}
				console.log('getMembersCommunity => membersCommunity : ',membersCommunity);
				resolve(membersCommunity);
      });
		} else {
			MembersCommunity
			.find({
				where: {
					id: idMembersCommunity
				}
			}, (err: any, membersCommunity: any) => {
				if (err) {reject(err);}
				console.log('getMembersCommunity => membersCommunity : ',membersCommunity);
				resolve(membersCommunity);
      });
		}
  });
}

//Promesa para actualizar datos en membersCommunity de acuerdo a una condicion especifica
export function updateMembersCommunity(
	cond: Object,
	updateDatos: Object
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    let MembersCommunity: any = app.models.PGQ_MembersCommunity;

		MembersCommunity.updateAll(cond,updateDatos,
		(err: any, updateCount: any) => {
			if (err) {  reject(err); }
			console.log('updateCount1 : ',updateCount);
			resolve(updateCount);
		});
	});
}

//Promesa para obtener la configuración de la comunidad
export function getConfigCommunity(
	idCommunity: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		let Config: any = app.models.PGQ_Config;
		Config.find({
			where: {
				fkCommunityId: idCommunity,
				status: true
			}
		}, (err: any, config: any) => {
				if (err) {reject(err);}

				if(config.length>0){
					resolve(config);
				} else {
					let error = 'Community has no configuration.';
					let errQrcode: any = new Error(error);
					errQrcode.statusCode = 400;
					reject(errQrcode);
				}
		});
  });
}

//Promesa para obtener el role del membersCommunity
export function getMembersCommunityRole(
	idUser: string,
	idCommunity: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    var MembersCommunity = app.models.PGQ_MembersCommunity;
    MembersCommunity
      .find({
        where: {
          fkUserId: idUser,
					fkCommunityId: idCommunity,
					status: true
        }
      }, (err: any, membersCommunity: any) => {
        if (err) {reject(err);}
					console.log('getMembersCommunityOther => membersCommunity : ',membersCommunity);
					if(membersCommunity.length>0) {
						getUserRoleCommunity(membersCommunity[0].id,true).then(roleCommunity => {
							console.log('roleCommunity................',roleCommunity);
							resolve(roleCommunity.name);
						});
					} else {
						resolve(false);
					}
      });
  });
}

//Promesa para obtener el members para la auto generación de codeQR
export function getMembersAutoGenerated(
	idUser: string,
	idCommunity: string,
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		let CodeQR: any = app.models.PGQ_QRCode;
		let deliveryDate: Date = new Date();
		let graceTime: Date = new Date();
		getGroupUserDefault(idUser,idCommunity).then(groupDefault => {
			console.log('groupDefault : ',groupDefault);
			getMembers(groupDefault,idUser,false,false).then(members => {
				console.log('members : ',members);
				console.log('HOLA');
				if(members.length>0) {
					resolve(members);
				}
			}, (err: any) => {
				reject(err);
			});
		},(err: any) => {
			reject(err);
		});
  });
}

//Promesa que obtiene usuario visitante
export function getUserVisitant(
	idCommunity: any,
	fkUserId: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    let MembersCommunity: any = app.models.PGQ_MembersCommunity;
    MembersCommunity
      .find({
        where: {
					fkUserId: fkUserId,
          fkCommunityId: idCommunity,
					status: true,
					check: false
        },
				include: {
					relation: 'membersRoleCommunity'
				}
      }, (err: any, membersCommunity: any) => {
        if (err) { reject(err); }
				console.log('getUserVisitant => membersRoleCommunity : ',membersCommunity);
        resolve(membersCommunity);
      });
  });
}

//Promesa que obtiene el members
export function getMembers(
	idGroup: any,
	idUser: any,
	status = true,
	check = false,
	move = false
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    let Members: any = app.models.PGQ_Members;
    Members
      .find({
        where: {
					fkUserId: idUser,
          fkGroupUserId: idGroup,
					status: status,
					check: check
        }
      }, (err: any, members: any) => {
        if (err) { reject(err); }
				console.log('getMembers => members : ',members);
				if (members.length > 0) {
						resolve(members);
				} else if(move===false) {
						let msgErr = 'User does not exist as a contact.';
						let errQrcode: any = new Error(msgErr);
						errQrcode.statusCode = 400;
						reject(errQrcode);
				} else {
					resolve([]);
				}
      });
  });
}

//Promesa que obtiene los codeQr activos por usuario
export function getMembersCodeQr(
	idUser: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    let Members: any = app.models.PGQ_Members;
    Members
      .find({
        where: {
					fkUserId: idUser,
					or: [
								{status: true,check: false},
								{status: false,check: false,type: 'autoCodeQR'},
					]
        },
				include: [
					{
						relation: 'codesQR',
						scope: {
							fields: ['code','timeExpire','deliveryDate','created'],
							where: {
								status: true
							}
						}
					},
					{
						relation: 'groupMembers',
						scope: {
							include: {
								relation: 'membersCommunity',
								scope: {
									include: [
										{
											relation: 'community',
											scope: {
												fields: ['address','location','name','id']
											}
										},
										{
											relation: 'user',
											scope: {
												fields: ['name','avatar','id']
											}
										}
									]
								}
							}
						}
					}
				]
      }, (err: any, members: any) => {
        if (err) {reject(err);}
				//console.log('members : ',members);
				//console.log(members[0].groupMembers);
				if (members.length > 0) {
					let result: Array<any> = [];
					members.forEach((element: any) => {
						//console.log('length : ',element.codesQR.length);
						if(element.codesQR().length>0) {
						let object: any = {};
							let objectCommunity : Object = element.groupMembers().membersCommunity().community();
							let objectUser : Object = element.groupMembers().membersCommunity().user();
							let objectCodeQr : Object = element.codesQR()[0];
							object.community = objectCommunity;
							object.user = objectUser;
							object.codesQR = objectCodeQr;
							result.push(object);
						}
					});

					if(result.length>0) {
						resolve(result);
					} else {
						let msgErr = 'User does not have active codeqr.';
						let errQrcode: any = new Error(msgErr);
						errQrcode.statusCode = 400;
						reject(errQrcode);
					}
				} else {
						let msgErr = 'User does not exist as a contact.';
						let errQrcode: any = new Error(msgErr);
						errQrcode.statusCode = 400;
						reject(errQrcode);
				}
      });
  });
}

//Promesa que se encarga de validar la notificación
export function validNotification(
	idUser: any,
	idUserRecipient: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    let Notification: any = app.models.PGQ_Notification;
    Notification
			.find({
        where: {
					fkUserId: idUserRecipient,
          fkUserIdSend: idUser,
					status: true,
					check: true,
					type: 7
        }
      }, (err: any, notification: any) => {
        if (err) {reject(err);}
				console.log('validNotification => notification : ',notification);
				if (notification.length > 0) {
						let msgErr = 'Active codeqr request.';
						let errQrcode: any = new Error(msgErr);
						errQrcode.statusCode = 400;
						reject(errQrcode);
				} else {
						resolve(true);
				}
      });
  });
}

/*Promesa que se encarga de crear grupo
 por defecto y crear al usuario como su mismo contacto */
export function createGroupMembersDefault(
	objectCtx: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    let GroupUser: any = app.models.PGQ_GroupUser;
		let Members: any = app.models.PGQ_Members;

			console.log('objectCtx : ',objectCtx);
			getUserRoleCommunity(objectCtx.id,true).then(membersRoleCommunity => {
				console.log('membersRoleCommunity : ',membersRoleCommunity);
				if(membersRoleCommunity) {
					console.log('Instance : ',objectCtx);
					GroupUser.create({
						created: new Date(),
						status: true,
						fkMembersCommunityId: objectCtx.id
					}, ( err: any, groupCreate: any ) => {
						if (err) { reject(err); }
						console.log('Grupo Creado : ',groupCreate);
						if(groupCreate) {
							Members.create({
								created: Date(),
								updated: Date(),
								check: false,
								status: false,
								//type: membersRoleCommunity.name,
								type: 'autoCodeQR',
								fkUserId: objectCtx.fkUserId,
								fkGroupUserId: groupCreate.id
							}, (err: any, members: any) => {
								if (err) {  reject(err); }
								console.log('members : ',members);
								if(members) {
									resolve(true);
								} else {
										let error = 'Automatic contact could not be created.';
										let errQrcode: any = new Error(error);
										errQrcode.statusCode = 400;
										reject(errQrcode);
								}
							});
						} else {
								let error = 'Default group could not be created.';
								let errQrcode: any = new Error(error);
								errQrcode.statusCode = 400;
								reject(errQrcode);
						}
					});
				}
			});
  });
}

//Promesa que se encarga de obtener el roleCommunity
export function getUserRoleCommunity(
	idMembersCommunity: any,
	status = true
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    let MembersCommunity: any = app.models.PGQ_MembersCommunity;
    MembersCommunity
      .find({
        where: {
					id: idMembersCommunity,
					status: status
        },
				include: {
					relation: 'membersRoleCommunity'
				}
      }, (err: any, membersCommunity: any) => {
        if (err) {reject(err);}
				console.log('getUserRoleCommunity.......................... : ',membersCommunity[0].membersRoleCommunity());
        resolve(membersCommunity[0].membersRoleCommunity());
      });
  });
}

//Promesa que desactiva la invitación
export function updateInvitation(
	id: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    let Invited = app.models.PGQ_Invited;
    Invited
      .updateAll({
					id: id,
          check: true
        },
				{
					check: false
				}, (err: any, updateInvited: any) => {
        if (err) {reject(err);}
					if(updateInvited.count > 0) {
						resolve(true);
					} else {
						resolve(false);
					}
      });
  });
}

//Estructura de parametro para creación del objecto habitante
interface Ihabitant {
		created: Date,
		updated: Date,
		status: boolean,
		locked: boolean,
		fkUserId: string,
		fkMembersCommunityId: string
};

//Promesa para crear o actualizar usuarios de tipo habitantes
export function createOrUpdateHabitant(
	objectHabitant: Ihabitant,
	type: boolean,
	id = ''
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		console.log('Interfaces : ',objectHabitant);
		let Habitant: any = app.models.PGQ_Habitant;
		if(type) {
			Habitant.create(
				objectHabitant
				,(err: any, habitantCreate: any) => {
					if (err) { throw err; }
					console.log('Habitante Creado ----- : ',habitantCreate);
					resolve(true);
			});
		} else {
				Habitant.updateAll({
					id: id,
					status: false,
					locked: false
				},{
					status: true,
					locked: false
				},(err, habitantUpdate) => {
					if (err) {throw err;}
					if(habitantUpdate.count>0) {
						console.log('Habitante Actualizado ----- : ',habitantUpdate);
						resolve(true);
					} else {
						resolve(false);
					}
				});
		}
  });
}

//Estructura de parametro para creación del objecto visitante
interface ImembersCommunity {
    created : Date,
    updated : Date,
    status : true,
    check : false,
    fkCommunityId : string,
    fkRoleCommunityId : string,
    fkUserId : string
};

//Promesa para crear o actualizar usuarios de tipo visitantes
export function createOrUpdateVisitant(
	objectMembersCommunity: ImembersCommunity,
	type: boolean,
	id = ''
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		console.log('createOrUpdateVisitant');
		console.log('Interface MembersCommunity : ',objectMembersCommunity);
		let MembersCommunity: any = app.models.PGQ_MembersCommunity;
		if(type) {
			//Paso # 1 -----INICIO-----
			console.log('Hola entro aqui');
			MembersCommunity.create(
				objectMembersCommunity
				,(err: any, visitantCreate: any) => {
					if (err) { reject(err); }
					console.log('Visitante Creado ----- : ',visitantCreate);
					if(visitantCreate) {
						getGroupUserDefault(objectMembersCommunity.fkUserId,
																objectMembersCommunity.fkCommunityId).then(idGroupDefault => {
							console.log('Se obtuvo el grupo : ',idGroupDefault);
								getMembersCommunity(id).then(resultMembersCommunity => {
										if (resultMembersCommunity.length > 0) {
											getGroupUserDefault(resultMembersCommunity[0].fkUserId,resultMembersCommunity[0].fkCommunityId)
												.then(idGroupDefaultOther => {
													console.log('Se obtuvo el segundo grupo : ',idGroupDefaultOther);
													console.log('VisitantCreate : ',visitantCreate);
												getUserRoleCommunity(visitantCreate.id).then(roleCommunity => {
													console.log('RoleCommunity 1 : ',roleCommunity);
													if(roleCommunity) {
														updateOrCreateContact(objectMembersCommunity.fkUserId,
																									idGroupDefaultOther,2,roleCommunity.name).then(membersCreate => {
															console.log('Paso 1');
															console.log('Creado contacto : ',membersCreate);
															if(membersCreate) {
																console.log('Paso 2');
																getUserRoleCommunity(resultMembersCommunity[0].id).then(roleCommunityOther => {
																	if(roleCommunityOther) {
																		updateOrCreateContact(resultMembersCommunity[0].fkUserId,
																												idGroupDefault,2,roleCommunityOther.name).then(membersCreateOther => {
																			console.log('Creado segundo contacto : ',membersCreateOther);
																			if(membersCreateOther) {
																				resolve(true);
																			}
																		});
																	}
																});
															}
														});
													}
												});
											});
										} else {
											resolve(false);
										}
								});
							});
						} else {
								resolve(true);
						}
				});
			} else {
					//-----------OJO FALTA ESTA PARTE----------
					MembersCommunity.updateAll({
						id: id,
						status: false,
						locked: false
					},{
						status: true,
						locked: false
					},(err, visitantUpdate) => {
						if (err) { reject(err); }
						if(visitantUpdate.count>0) {
							console.log('Visitante Actualizado ----- : ',visitantUpdate);
							resolve(true);
						} else {
							resolve(false);
						}
					});
			}
  });
}

//Promesa que se encarga de agregar el usuario dependiendo del tipo de rol community
export function createUserTypeCommunity(
	objectInvited: any,
	objectUser: any,
	status = false,
	locked = true
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {

		console.log('createUserTypeCommunity');
		console.log('createUserTypeCommunity => Invitación Promise : ',objectInvited);
		console.log('createUserTypeCommunity => Invitación User Promise : ',objectUser);

		console.log('length Invited : ',objectInvited.length);
		console.log('length User : ',objectUser.length);
		if (objectInvited && objectUser) {
			console.log('Entro');
			/*
			//Creación de estructura de interfaces
			let habitant: Ihabitant;
			habitant	= {
				created: new Date(),
				updated: new Date(),
				status: true,
				locked: false,
				fkUserId: objectUser.id,
				fkMembersCommunityId: objectInvited.fkMembersCommunityId
			};
			if (objectInvited.type==='Habitant') {
				getUserHabitant(objectInvited.fkMembersCommunityId,objectUser.id).then(habitantObject => {
					if(habitantObject.length > 0) {
						createOrUpdateHabitant(habitant,false,habitantObject[0].id).then(resultHabitant => {
								console.log('-----------------------Habitante Existente-------------------');
								resolve(resultHabitant);
						});
					} else {
						createOrUpdateHabitant(habitant,true).then(resultHabitant => {
								console.log('-----------------------Habitante No Existente-------------------');
								resolve(resultHabitant);
						});
					}
				});
			} else*/
			 if (objectInvited.type==='Visitant' || objectInvited.type==='Resident' || objectInvited.type==='Habitant') {
					console.log('Type : ',objectInvited.type);
					let MembersCommunity: any = app.models.PGQ_MembersCommunity;

					console.log('objectInvited : ',objectInvited);

					MembersCommunity.find({
						where: {
							id: objectInvited.fkMembersCommunityId
						}
					},(err: any, membersCommunity: any) => {
							if (err) { reject(err); }

							console.log('membersCommunity : ',membersCommunity);

							if(membersCommunity.length > 0) {
								getRoleCommunity(objectInvited.type).then(roleCommunity => {
									if(roleCommunity.length > 0) {
											//Preparación de objeto interfaces
											let objectMembersCommunity: ImembersCommunity;
											objectMembersCommunity = {
												created : new Date(),
												updated : new Date(),
												status : true,
												check : false,
												fkCommunityId : membersCommunity[0].fkCommunityId,
												fkRoleCommunityId : roleCommunity[0].id,
												fkUserId : objectUser.id
											};
											//Preparación de objeto interfaces
											console.log('objectMembersCommunity : ',objectMembersCommunity);
											getUserVisitant(membersCommunity[0].fkCommunityId,objectUser.id).then(resultUserVisitant => {
												console.log('******resultUserVisitant******* : ',resultUserVisitant);
												if(resultUserVisitant.length > 0) {
													createOrUpdateVisitant(objectMembersCommunity,false,objectInvited.fkMembersCommunityId).then(resultVisitant => {
															console.log('resultVisitant : ',resultVisitant);
															console.log('-----------------------Existente-------------------',objectInvited.type);
															resolve(resultVisitant);
													});
												} else {
														createOrUpdateVisitant(objectMembersCommunity,true,objectInvited.fkMembersCommunityId).then(resultVisitant => {
															console.log('-----------------------No Existente-------------------',objectInvited.type);
															resolve(resultVisitant);
														});
												}
											});
									}
								});
							} else {
									let error = 'Requesting member does not exist.';
									let errQrcode: any = new Error(error);
									errQrcode.statusCode = 400;
									reject(errQrcode);
							}
					});
			}
		} else {
				console.log('ELSE');
				resolve(false);
		}
  });
}

//Promesa para obtener datos de usuario y su respectiva comunidad
export function getUserCommunity(
	idGroup: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    let GroupUser: any = app.models.PGQ_GroupUser;
    GroupUser
      .find({
        where: {
          _id: idGroup
        }
      }, (err: any, result: any) => {
        if (err) { reject(err); }
        let MembersCommunity: any = app.models.PGQ_MembersCommunity;
        MembersCommunity
          .find({
            where: {
              _id: result[0].fkMembersCommunityId
            },
            include: [
              {relation: 'user'},
              {relation: 'community'}
            ]
          },(err: any, membersCommunity: any) => {
            if (err) { reject(err); }
						console.log("membersCommunity : ",membersCommunity);
						if(membersCommunity.length > 0) {
							resolve(membersCommunity);
						} else {
							let error = 'User does not exist in the community.';
							let errQrcode: any = new Error(error);
							errQrcode.statusCode = 400;
							reject(errQrcode);
						}
          });
      });
  });
}

//Promesa para obtener los datos del grupo por defecto del usuario
export function getGroupUserDefault(
	idUser: any,
	idCommunity: any
): Promise<string> {
  return new Promise((resolve: any, reject: any) => {
		let MembersCommunity = app.models.PGQ_MembersCommunity;
		MembersCommunity
			.find({
				where: {
					fkUserId: idUser,
					fkCommunityId: idCommunity,
					status: true
				}
			},(err: any, membersCommunity: any) => {
				if (err) {reject(err);}

				console.log('membersCommunity : ',membersCommunity);

				if(membersCommunity.length > 0){
					let GroupUser = app.models.PGQ_GroupUser;
					GroupUser
						.find({
							where: {
								fkMembersCommunityId: membersCommunity[0].id,
								name: 'default',
								status: true
							}
						},(err: any, groupUser: any) => {
							if(groupUser.length > 0) {
								resolve(groupUser[0].id);
							} else {
								  /*Creación de group por defecto en caso
									 que el miembro de la comunidad no lo tenga*/
									GroupUser.create({
										created: new Date(),
										updated: new Date(),
										fkMembersCommunityId: membersCommunity[0].id
									}, (err: any, createGroup: any) => {
											if (err) {reject(err);}
											resolve(createGroup.id);
									})
							}
						});
				} else {
						let error = 'User does not belong to the community.';
						let errQrcode: any = new Error(error);
						errQrcode.statusCode = 400;
						reject(errQrcode);
				}
			});
	});
}

//Promesa para obtener los datos del grupo o los grupos de un usuario de una comunidad
export function getGroupUser(
	idMembersCommunity: any,
	nameGroup: any
): Promise<string> {
  return new Promise((resolve: any, reject: any) => {
		let MembersCommunity = app.models.PGQ_MembersCommunity;
		MembersCommunity
			.find({
				where: {
					_id: idMembersCommunity,
					status: true
				}
			},(err: any, membersCommunity: any) => {
				if (err) {reject(err);}

				console.log('getGroupUser->membersCommunity : ',membersCommunity);

				if(membersCommunity.length > 0){
					let GroupUser = app.models.PGQ_GroupUser;
					GroupUser
						.find({
							where: {
								fkMembersCommunityId: membersCommunity[0].id,
								name: nameGroup,
								status: true
							}
						},(err: any, groupUser: any) => {
							if(groupUser.length > 0) {
								resolve(groupUser[0].id);
							} else {
								let error = 'Group does not exist.';
								let errQrcode: any = new Error(error);
								errQrcode.statusCode = 400;
								reject(errQrcode);
							}
						});
				} else {
						let error = 'User does not belong to the community.';
						let errQrcode: any = new Error(error);
						errQrcode.statusCode = 400;
						reject(errQrcode);
				}
			});
	});
}

//Promesa que se encarga de eliminar contacto de ambos grupos y codeQR
//Nota: Integrar este servicio con la eliminación de los codeQR de los habitantes
export function deleteGroupContact(
	idUserContact: any,
	idGroup: any
): Promise<string> {
  return new Promise((resolve: any, reject: any) => {
		let Members: any = app.models.PGQ_Members;
		idUserContact = new ObjectID(idUserContact);
		idGroup = new ObjectID(idGroup);

		Members.find({
			where:{
				status: true,
				fkUserId: idUserContact,
				fkGroupUserId: idGroup
			}
		}, (err,members) => {
			if (err) {throw err;}
			if (members.length > 0) {
				Members.updateAll({
						status: true,
						fkUserId: idUserContact,
						fkGroupUserId: idGroup
				},{
						status: false,
						check: false
				},(err,result)=> {
						if (err) {reject(err);}
						if (result.count > 0){
								let QRCode: any = app.models.PGQ_QRCode;
								QRCode.updateAll({
										status: true,
										fkMembersId: members[0].id
								},{
										status: false
								},(err,deleteCodeQR)=> {
										if (err) {reject(err);}
										resolve(true);
								});
						} else {
							reject(false);
						}
				});
			} else {
					reject(false);
			}
		});
	});
}

//Promesa para obtener el Id del usuario lider del grupo
export function getUserId(
	idGroup: any
): Promise<string> {
  return new Promise((resolve: any, reject: any) => {
    var GroupUser = app.models.PGQ_GroupUser;
    GroupUser
      .find({
        where: {
          _id: idGroup,
					status: true
        }
      }, (err: any, result: any) => {
        if (err) {reject(err);}
        var MembersCommunity = app.models.PGQ_MembersCommunity;
        MembersCommunity
          .find({
						fields: ['fkUserId'],
            where: {
              _id: result[0].fkMembersCommunityId,
							status: true
            }
          },(err: any, userId: any) => {
            if (err) {reject(err);}
            resolve(userId[0].fkUserId);
          });
      });
  });
}

//Promesa para obtener datos de una comunidad
export function getCommunity(
	idGroup: any
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
    var GroupUser = app.models.PGQ_GroupUser;
    GroupUser
      .find({
        where: {
          _id: idGroup
        }
      }, (err: any, result: any) => {
        if (err) {reject(err);}
        var MembersCommunity = app.models.PGQ_MembersCommunity;
        MembersCommunity
          .find({
            where: {
              _id: result[0].fkMembersCommunityId
            },
          },(err: any, membersCommunity: any) => {
            if (err) {reject(err);}
            var Community = app.models.PGQ_Community;
            Community
              .find({
                fields: ['name','id','address','country','state'],
                where: {
                  _id: membersCommunity[0].fkCommunityId
                }
              }, (err: any, community: any) => {
                if (err) {reject(err);}
                resolve(community);
              });
          });
      });
  });
}

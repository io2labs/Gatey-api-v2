//Declaración de constantes
const crypto = require('crypto');
//Declaración de variables
let algorithm = 'aes-256-ctr';
let password = 'd6F3Efeq';

//Promesa que se encarga de crear un code encriptado
export function createCodeEncrypt(
	idCodeQr: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		let cipher = crypto.createCipher(algorithm,password);
		let crypted = cipher.update(idCodeQr.toString(),'utf8','hex');
		crypted += cipher.final('hex');
		resolve(crypted);
  });
}

//Promesa que se encarga de desencriptar code
export function codeDecrypt(
	idCodeQr: string
): Promise<any> {
  return new Promise((resolve: any, reject: any) => {
		let decipher = crypto.createDecipher(algorithm,password)
		let codeQrDec = decipher.update(idCodeQr,'hex','utf8')
		codeQrDec += decipher.final('utf8');
		resolve(codeQrDec);
  });
}

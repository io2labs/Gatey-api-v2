import {
    Model
} from '@mean-expert/model';

/**
 * Modelo PGQ_Groupuser
 *
 * @class PgqGroupUser
 */
@Model({
    hooks: {
        afterSave:  { name: 'after save', type: 'operation'},
        access:{ name: 'access', type: 'operation'}
    },
    remotes: {}
})
class PgqGroupUser {
		/**
		 * Creates an instance of PgqGroupUser.
		 *
		 * @param {*} reference
		 *
		 * @memberOf PgqGroupUser
		 */
    constructor(reference: any) { }

		/**
		 *
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqGroupUser
		 */
    static afterSave(ctx: any, next: Function): void {
        //console.log('PgqGroupUser: After Save');
        // var typeEvent;
        // var Audit = ctx.Model.app.models.PGQ_Audit;
        // if (ctx.isNewInstance===true){
        //     typeEvent = 'Create';
        // }else{
        //     typeEvent = 'Update';
        // }
        // if (ctx.instance) {
        //     Audit.generateAudit(ctx.Model.modelName,typeEvent, ctx.query, ctx.instance);
        // }
        next();
    }

		/**
		 *
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqGroupUser
		 */
    static access(ctx: any, next: Function): void {
        //console.log('PgqGroupUser: Access');
        // var Audit = ctx.Model.app.models.PGQ_Audit;
        // Audit.generateAudit(ctx.Model.modelName,'Find',ctx.query);
        next();
    }
}
module.exports = PgqGroupUser;

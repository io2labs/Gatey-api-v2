import {
    Model
} from '@mean-expert/model';

/**
 * Modelo PGQ_Countries
 *
 * @class PgqCountries
 */

@Model({
    hooks: {},
    remotes:{}
})

class PgqCountries {
	/**
	 * Creates an instance of PgqCountries.
	 *
	 * @param {*} reference
	 *
	 * @memberOf PgqCountries
	 */
    constructor(reference: any) { }
}
module.exports = PgqCountries;

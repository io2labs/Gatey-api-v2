import {
    Model
} from '@mean-expert/model';

import {
	getInvited,
	getCheckUser,
	createUserTypeCommunity
} from '../../server/helpers';

const app = require('../../server/server');
const path = require('path');
const config = require('../../server/config.json');
const datasource = require('../../server/datasources.json');

/**
 * Modelo PGQ_User
 *
 * @class PgqUser
 */
@Model({
    hooks: {
        beforeSave: { name: 'before save', type: 'operation' },
        afterSave:  { name: 'after save', type: 'operation' },
        afterRemote:{ name: 'create', type: 'afterRemote' },
        resetPass:{name: 'resetPasswordRequest', type: 'afterRemote' }
    },
    remotes: {
        passwordReset: {
            accepts : [ {arg: 'email', type: 'string', required: true }],
            returns : { arg: 'success', type: 'string' },
            http    : { path:'/passwordReset', verb: 'post' }
        }
    }
})
class PgqUser {
		/**
		 * Creates an instance of PgqUser.
		 *
		 * @param {*} reference
		 *
		 * @memberOf PgqUser
		 */
    constructor(reference: any) {
			//let User: any = app.models.PGQ_User;
			//User.resetPassword.on('resetPasswordRequest', PgqUser.resetPass);
			//PgqUser.passwordReset.on('resetPasswordRequest', PgqUser.resetPass);
        //reference.on('resetPasswordRequest', PgqUser.resetPass); //ojo con esto
				//PgqUser.on('resetPasswordRequest', PgqUser.resetPass);
				//User.passwordReset.on('resetPasswordRequest', PgqUser.resetPass);
    }

		/**
		 *
		 *
		 * @static
		 * @param {string} email
		 * @param {Function} next
		 *
		 * @memberOf PgqUser
		 */
    static passwordReset (email: string, next: Function): void {
        let User = app.models.PGQ_User;
        User.resetPassword({
            email: email
        }, (err: any) => {
            if (err) {throw err;}
            var content = 'Check your email for further instructions';
            next(null,content);
        });
    }

		/**
		 * Hook que validad que la comunidad exista
		 * antes de guardar el usuario
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqUser
		 */

    static beforeSave (ctx: any, next: Function): void {
        console.log('PgqUser: Before Save');
				console.log('Instance : ',ctx.instance);
				console.log('Operacion : ',ctx.isNewInstance);
				console.log('--------------------------------------------------');
				let Invited: any = ctx.Model.app.models.PGQ_Invited;
        let jsonUser: any = ctx.instance;

         //Con esta variable se control se esta creando o actualizando un registro
        var typeEvent: boolean = ctx.isNewInstance;
        if(typeEvent===true) {
            let code: string = jsonUser.codeTemp;
						let email: string = jsonUser.email;

						getInvited(code,email).then(objectInvited => {
							//console.log('Invited : ',objectInvited);
							if (objectInvited.length>0 && typeEvent===true){
								getCheckUser(email).then(objectUser => {
									//console.log('User : ',objectUser);
									if (objectUser.length>0) {
										let error = 'There is an account with this same email.';
										let errQrcode: any = new Error(error);
										errQrcode.statusCode = 400;
										next(errQrcode);
									} else {
										next();
									}
								});
							}else if(objectInvited.length===0 && typeEvent===true){
									next(new Error('Invitation code does not exist.'));
							}else if(typeEvent === undefined){
									next();
							}
						});

        } else {
				 	next();
      	}
    }

		/**
		 * Hook que se encarga de asociar
		 * el usuario a la comunidad
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqUser
		 */

    static afterSave(ctx: any, next: Function): void {
        console.log('PgqUser: After Save');
				console.log('Instance : ',ctx.instance);
				var typeEvent: boolean = ctx.isNewInstance;
				console.log('TypeEvent : ',typeEvent);
        if (typeEvent===true) {
					getInvited(ctx.instance.codeTemp,ctx.instance.email).then(objectInvited => {
						createUserTypeCommunity(objectInvited[0],ctx.instance).then(result => {
						console.log('afterSave Promise : ',objectInvited);
						console.log(result);
						next();
						});
					});
				} else {
						console.log('Update');
						console.log('Intance : ',ctx.instance);
						console.log('Intance Where : ',ctx.where);
						next();
				}
    }
		/**
		 *
		 *
		 * @static
		 * @param {*} info
		 *
		 * @memberOf PgqUser
		 */
    static resetPass(info: any) {
        var baseUrl = `http://${config.domain}:${config.port}`;
        var url = `${baseUrl}/reset-password?access_token=${info.accessToken.id}`;
        // var html = 'Click <a href="' + url + '?access_token=' +
        // info.accessToken.id + '">here</a> to reset your password';
        var html =  `
        <link rel="stylesheet" href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
              integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
              crossorigin="anonymous">
        <link rel="stylesheet" href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css'
              integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r"
              crossorigin="anonymous">
        <style>
            body{
                    background-color: #009688;
                }
            .formGatey{
                background-color: rgba(255,255,255,0.8);
                padding: 10px;
                margin-top: 50px;
                width: auto;
            }
            .resetPass{
                margin: 10px;
            }
            .inputGatey{
                margin-bottom: 20px;
            }
            .imgLogo{
                width: 200px;
                height: auto;
                margin-top: 50px;
            }
        </style>
        <section>
            <center>
                <img class="imgLogo" src="http://54.210.123.160:3000/image/logo.png" alt="">
            </center>
            <div class="container">
                <fieldset class="formGatey">
                <div class="row">
                    <div class="col-md-12 text-center">
                    <legend><h1>Password reset</h1></legend>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-offset-4 col-md-4 inputGatey">
                    <center><label>You have request a password reset</label></center>
                    </div>
                </div>
                <div class="col-md-12 text-center resetPass">
                    <a  class="btn btn-success" href="${url}">Reset password</button>
                </div>
                </fieldset>
            </div>
        </section>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
                integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
                crossorigin="anonymous"></script>`;
        // aqui quede probar
        let Email = app.models.Email;
        Email.send({
            to: info.email,
            from: datasource.emailDs.transports[0].auth.user,
            subject: 'Password reset (Gatey)',
            text: '',
            html: html
        }, (err: any, mail: any)=> {
            if (err) {
                return console.log('> error sending password reset email');
            } else {
                console.log('> sending password reset email to:', mail);
            }
        });
    }

		/**
		 * send verification email after registration
		 *
		 * @static
		 * @param {*} context
		 * @param {*} userInstance
		 * @param {Function} next
		 *
		 * @memberOf PgqUser
		 */
    static afterRemote(context: any, userInstance: any, next: Function) {
        console.log('> user.afterRemote triggered');
        var contextHtml = `
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
              integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
              integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r"
              crossorigin="anonymous">
        <style>
            body{
                background: rgb(0, 150, 136);
                background-size: cover;
            }
            .formGatey{
                background-color: rgba(255,255,255,0.8);
                padding: 10px;
                margin-top: 50px;
                width: auto;
            }
            .resetPass{
                margin: 10px;
            }
            .inputGatey{
                margin-bottom: 20px;
            }
            .imgLogo{
                width: 200px;
                height: auto;
                margin-top: 50px;
            }
        </style>
        <section>
            <center>
                <img class="imgLogo" src="http://ec2-54-210-123-160.compute-1.amazonaws.com:3000/image/logo.png" alt="">
            </center>
            <div class="container">
                <fieldset class="formGatey">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <legend><h1>Welcome to gatey</h1></legend>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-4 col-md-4 inputGatey">
                            <center><label>In order to complete your account, please verify your email by clicking this link:</label></center>
                        </div>
                    </div>
                    <div class="col-md-12 text-center resetPass">
                        <a class="btn btn-primary" href="">Verify</button>
                    </div>
                </fieldset>
            </div>
        </section>
        <script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                crossorigin="anonymous"></script>`;

        var options = {
            host: datasource.configHost.domain,
            type: 'email',
            to: userInstance.email,
            from: datasource.emailDs.transports[0].auth.user,
            subject: 'Successful registration (Gatey).',
            template: path.resolve(__dirname, '../../server/views/verify.ejs'),
            redirect: '/verified',
            user: PgqUser,
            html: contextHtml
        };

        userInstance.verify(options, (err: any, response: any)=> {
            if (err) { next(err); }
            //console.log('> verification email sent:', response);
            next();
        });
    }
}
module.exports = PgqUser;

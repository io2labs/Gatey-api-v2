import {
    Model
} from '@mean-expert/model';


/**
 * Modelo PGQ_Provinces
 *
 * @class Pgqprovinces
 */
@Model({
    hooks: {},
    remotes: {}
})
class Pgqprovinces {
		/**
		 * Creates an instance of Pgqprovinces.
		 *
		 * @param {*} reference
		 *
		 * @memberOf Pgqprovinces
		 */
    constructor(reference: any) {  }
}
module.exports = Pgqprovinces;

import {
		Model
} from '@mean-expert/model';


/**
 * Modelo PGQ_Config
 *
 * @class PgqConfig
 */
@Model({
		hooks: {
				access: { name: 'access', type: 'operation'},
				afterSave:  { name: 'after save', type: 'operation'}
		},
		remotes: {}
})
class PgqConfig {
	/**
	 * Creates an instance of PgqConfig.
	 *
	 * @param {*} reference
	 *
	 * @memberOf PgqConfig
	 */
		constructor(reference: any) { }

		/**
		 *
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqConfig
		 */
		static afterSave(ctx: any, next: Function): void {
				// console.log('PgqConfig: After Save');
				// var typeEvent;
				// var Audit = ctx.Model.app.models.PGQ_Audit;
				// if (ctx.isNewInstance===true){
				//     typeEvent = 'Create';
				// }else{
				//     typeEvent = 'Update';
				// }
				// if (ctx.instance) {
				//     Audit
				//     .generateAudit(ctx.Model.modelName,typeEvent,
				//                    ctx.query,
				//                    ctx.instance);
				// }
				next();
		}

		/**
		 *
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqConfig
		 */
		static access(ctx: any, next: Function): void {
				//console.log('PgqConfig: Access');
				// se debe revisar este codigo ya que guarda muchos registros
				// var Audit = ctx.Model.app.models.PGQ_Audit;
				// Audit.generateAudit(ctx.Model.modelName,'Find',ctx.query);
				next();
		}
}
module.exports = PgqConfig;

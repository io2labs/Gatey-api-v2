import {
	Model
} from '@mean-expert/model';

import {
	getCodeRandom, getRoleCommunity,
	getCheckUser, getGroupUserDefault,
	getCommunity, notificationContact,
	checkInvitation, createInvitation,
	sendEmail, validationTypeContact,
	getUserId, getMembers, validNotification,
	getUserCommunity, getUserRoleCommunity,
	getMembersCommunityRole, validUserCommunityContact,
	getMembersCommunity, updateMembersCommunity
} from '../../server/helpers';

const app = require('../../server/server');
const datasource = require('../../server/datasources.json');
const ObjectID = require('mongodb').ObjectID;

/**
 * Modelo PGQ_Invited
 *
 * @class PgqInvited
 */
@Model({
	hooks: {},
	remotes: {
		invitation: {
			accepts: [
				{ arg: 'idUser', type: 'string', required: true },
				{ arg: 'idCommunity', type: 'string', required: true },
				{ arg: 'email', type: 'string', required: true },
				{ arg: 'type', type: 'string', required: true }
			],
			returns: { root: true, type: 'boolean' },
			http: { path: '/invitation', verb: 'post' }
		},
		requestCodeQR: {
			accepts: [
					{ arg: 'idGroup', type: 'string' },
					{ arg: 'idUserContact', type: 'string' },
					{ arg: 'deliveryDate', type: 'date' },
					{ arg: 'graceTime', type: 'date' }
			],
			returns:{ root: true, type: 'booleam' },
			http:{ path: '/requestCodeQR', verb: 'post' }
	}
	}
})
class PgqInvited {
	/**
	 * Creates an instance of PgqInvited.
	 *
	 * @param {*} reference
	 *
	 * @memberOf PgqInvited
	 */
	constructor(reference: any) { }

	/**
	 * Metodo remoto que se encarga de registrar una invitación
	 * Nota: mejorar el tema de los roleCommunity dinamicos.
	 *
	 * @static
	 * @param {string} idUser
	 * @param {string} emailInvited
	 * @param {string} idRoleInvited
	 * @param {Function} next
	 *
	 * @memberOf PgqInvited
	 */
	static invitation(
		idUser: string,
		idCommunity: string,
		email: string,
		type: string,
		next: Function): void {

		let MembersCommunity: any = app.models.PGQ_MembersCommunity;
		MembersCommunity.find({
			where: {
				fkUserId: idUser,
				fkCommunityId: idCommunity,
				status: true
			},
			include: [
				{ relation: 'community' },
				{ relation: 'user' }
			]
		}, (err: any, membersCommunity: any) => {
			if (err) {  next(err,false); }
			console.log('MembersCommunity........ : ', membersCommunity);
			if (membersCommunity.length === 1) {
				checkInvitation(type, email, membersCommunity[0].id).then(resultInvitation => {
					if (resultInvitation.length > 0) {
						let error = 'Active invitation for this user.';
						let errQrcode: any = new Error(error);
						errQrcode.statusCode = 400;
						next(errQrcode, false);
					} else {
						console.log('hola');
						getCheckUser(email).then(resultUser => {
							console.log('Check User : ', resultUser);
							if (resultUser.length > 0) {
								//console.log(resultUser[0].id,membersCommunity[0].community().id);
								validUserCommunityContact(resultUser[0].id,membersCommunity[0].community().id).then(datosContact => {
									getMembersCommunityRole(resultUser[0].id,membersCommunity[0].community().id).then(typeMembersCommunity => {
										console.log('typeMembersCommunity : ',typeMembersCommunity);
										console.log('datosContact : ',datosContact);
										if(typeMembersCommunity && datosContact.length>0) {
											//if(typeMembersCommunity!==type) {
												let error = 'User already exists in this community as '+typeMembersCommunity;
												let errQrcode: any = new Error(error);
												errQrcode.statusCode = 400;
												next(errQrcode, false);
											//}
										} else if(datosContact.length===0) {
											  console.log('Entro');
												getMembersCommunity('',true,resultUser[0].id,membersCommunity[0].community().id).then(objMembersCommunity => {
													console.log('objMembersCommunity : ',objMembersCommunity);
													console.log(objMembersCommunity.length);
														if (objMembersCommunity.length===0) {
															objMembersCommunity[0] = {
																id: ''
															};
														}
														updateMembersCommunity(
															{
																id: objMembersCommunity[0].id,
																status: true,
																check: false
															},
															{
																status: false,
																check: false
															}).then(updateCount => {
															  console.log('updateCount2 : ',updateCount);
																console.log(type);
																console.log(resultUser[0].id);
																console.log(membersCommunity[0].id);
																console.log('community: ',membersCommunity[0].community().id);
																validationTypeContact(type, resultUser[0].id, membersCommunity[0].community().id, membersCommunity[0].id).then(validContact => {
																console.log('Validacion contacto : ',validContact);
																if (validContact) {
																	//Proceso para generar notificación de invitación
																		createInvitation(type, email, membersCommunity[0].id).then(invited => {
																		if (invited) {
																			getCheckUser(email).then(resultUser => {
																				console.log('Resultado User : ', resultUser);
																				if (resultUser.length > 0) {
																					getGroupUserDefault(idUser, idCommunity).then(groupId => {
																						if (groupId) {
																							getCommunity(groupId).then(communityObject => {
																								if (communityObject.length > 0) {
																									notificationContact(
																										idUser, resultUser[0].id, 5,
																										'Wanna add you', invited, communityObject[0]
																									).then(result => {
																										if (result) {
																											next(null, true);
																										} else {
																											next(null, false);
																										}
																									});
																								} else {
																									next(null, false);
																								}
																							});
																						} else {
																							next(null, false);
																						}
																					});
																				} else {
																					next(null, false);
																				}
																			});
																		} else {
																			next(invited, false);
																		}
																	}, (err: any) => {
																		next(err);
																	});
																	//Proceso para generar notificación de invitación
																}
															}, (err) => {
																next(err);
															});
														}, (err: any) => {
															next(err);
														});

												});
										}
									}, (err: any) => {
										next(err);
									});
								}, (err: any) => {
									next(err);
								});
							} else {
								console.log('Invitación con correo'),
									//Proceso que genera la invitación y la envia por correo electronico
									createInvitation(type, email, membersCommunity[0].id).then(invited => {
										if (invited) {
											let html: string = `<link rel="stylesheet" href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
																			integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
																			crossorigin="anonymous">
															<link rel="stylesheet" href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css'
																			integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r"
																			crossorigin="anonymous">
															<style>
															</style>
															<section style="background-color: #26736B;">
																	<center>
																			<img style="width: 20%; height: auto;" src="http://ec2-54-210-123-160.compute-1.amazonaws.com:3000/image/logo.png" alt="">
																			<h1 style="color: white; font-size: 3em; font-weight: bolder; width: 70%;">GATEY</h1>
																	</center>
																	<div class="container" style="margin-bottom: 10px;">
																			<fieldset style="background-color: rgba(255,255,255,0.8); padding: 10px; margin-top: 50px; width: auto;">
																					<div class="row">
																							<div class="col-md-12 text-center">
																									<legend><h1>Invitation</h1></legend>
																							</div>
																					</div>
																					<div class="row">
																							<div class="col-md-12 text-center" style="margin-bottom: 20px;">
																									<center>
																											<p>
																													<h2>You have been intived</h2>
																													<p> <strong> ${membersCommunity[0].user().name} </strong> has just send you an invitation to join to his community <strong> "${membersCommunity[0].community().name}" </strong></p><br>
																													<p>You have been invite as <strong> ${type} </strong></p><br>
																													<p>Here is your code: <strong> ${invited.code} </strong></p><br> <br>
																													<a href="#">Download our app</a>
																											</p>
																									</center>
																							</div>
																					</div>
																			</fieldset>
																	</div>
															</section>
															<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js"></script>
															<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
																			integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
																			crossorigin="anonymous">
															</script>`;
											sendEmail(email, html, 'Invitation (Gatey)', '').then(resulSendEmail => {
												if (resulSendEmail) {
													next(null, true);
												} else {
													next(resulSendEmail, false);
												}
											});
										} else {
											next(invited);
										}
									});
								//Proceso que genera la invitación y la envia por correo electronico
							}
						});
					}
				});
			} else if (membersCommunity.length > 1) {
				let error = 'The user belongs to more than one community.';
				var errQrcode: any = new Error(error);
				errQrcode.statusCode = 400;
				next(errQrcode, false);
			} else {
				let error = 'The user is not assigned community.';
				let errQrcode: any = new Error(error);
				errQrcode.statusCode = 400;
				next(errQrcode, false);
			}
		});
	};

	/**
	 * Servicio que se encarga de enviar una notificacióon de solicitud de envio de codeQR
	 *
	 * @static
	 * @param {string} idmembers
	 * @param {Function} next
	 *
	 * @memberOf PgqInvited
	 */
	static requestCodeQR(
		idGroup: string,
		idUserContact: string,
		deliveryDate: Date,
		graceTime: Date,
		next: Function): void {
				idGroup = new ObjectID(idGroup);
				idUserContact = new ObjectID(idUserContact);

			//--------------Generar notificación----------------
				getUserId(idGroup).then(userId => {
					validNotification(userId,idUserContact).then(validRequest => {
						if(validRequest) {
							getCommunity(idGroup).then(communityObject => {
								getUserCommunity(idGroup).then(userCommunity => {
									console.log('userCommunity : ',userCommunity[0]);
									console.log('communityObject : ',communityObject[0]);
									if(userCommunity.length>0) {
										getGroupUserDefault(idUserContact,communityObject[0].id).then(idGroupRequest => {
											console.log('idGroup : ',idGroupRequest);
											console.log('idUser : ',userCommunity[0].user().id);
											getMembers(idGroupRequest,userCommunity[0].user().id).then(members => {
												if(members.length>0) {
													members[0].deliveryDate = deliveryDate;
													members[0].graceTime = graceTime;
													getUserId(idGroup).then(userId =>  {
														notificationContact(
															userId,idUserContact,7,
															'Request for codeQR',members[0],communityObject[0]
														).then(result => {
															if(result){
																	next(null,true);
															} else{
															next(null,false);
															}
														});
													});
												}
											},(err: any) => {
												next(err, false);
											});
									});
									}
								}, (err: any) => {
										next(err, false);
								});
							});
						}
					}, (err: any) => {
						next(err,false);
					});
				});
			//--------------Generar notificación----------------
	};
}
module.exports = PgqInvited;

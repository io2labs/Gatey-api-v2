import {
	Model
} from '@mean-expert/model';

const ObjectID = require('mongodb').ObjectID;
const app = require('../../server/server');
/**
* @class PgqCommunity
*
* Se elimina el metodo remoto getCommunity por redundar
* a un metodo del mismo framework
***
* se obvio un metodo para enviar correo despudes de registro master de la comunidad
* estaba comentado
**/
@Model({
	hooks: {
		beforeSave: { name: 'before save', type: 'operation' }
	},
	remotes: {
		listCommunity: {
			accepts: [
				{ arg: 'idUser', type: 'string' }
			],
			returns: { root: true, type: 'array' },
			http: { path: '/listCommunity', verb: 'post' }
		},
		communitiesCommon: {
			accepts: [
				{ arg: 'idUser', type: 'string' },
				{ arg: 'idUserContact', type: 'string' }
			],
			returns: { root: true, type: 'array' },
			http: { path: '/communitiesCommon', verb: 'post' }
		}
	}
})
class PgqCommunity {
	/**
	 * Creates an instance of PgqCommunity.
	 *
	 * @param {*} reference
	 *
	 * @memberOf PgqCommunity
	 */
	constructor(reference: any) { }

	/**
	 * Metodo remoto que se encarga de obtener el role del usuario ??
	 *
	 * @static
	 * @param {string} idUser
	 * @param {Function} next
	 *
	 * @memberOf PgqCommunity
	 */
	static listCommunity(idUser: string, next: Function): void {
		let Community = app.models.PGQ_Community;
		let MembersCommunity = app.models.PGQ_MembersCommunity;
		let Habitant: any = app.models.PGQ_Habitant;
		idUser = new ObjectID(idUser);

		MembersCommunity.find({
			fields: {
				fkCommunityId: true
			},
			where: {
				status: true,
				fkUserId: idUser
			}
		}, (err, communitiesMembers) => {
			if (err) { next(err); }
			console.log(communitiesMembers);
			var datos: Array<any> = [];
			var object: any = {};
			for (var i = 0; i < communitiesMembers.length; i++) {
				object = {};
				object.id = new ObjectID(communitiesMembers[i].fkCommunityId);
				datos.push(object);
			}
			console.log('Datos : ',datos);
/*
			Habitant.find({
				where: {
					fkUserId: idUser,
					status: true
				},
				include: {
					relation: 'membersCommunity'
				}
			}, (err: any, resultHabitant: any) => {
					if (err) { throw err; }

					console.log('Habitante : ', resultHabitant);

					if(resultHabitant.length>0){
						for (let i = 0; i < resultHabitant.length; i++) {
							object = {};
							object.id = new ObjectID(resultHabitant[i].membersCommunity().fkCommunityId);
							datos.push(object);
						}
					}*/
			//datos.push({ ownerId: idUser });
			// console.log(datos);
			if(datos.length>0) {
					Community.find({
						fields: ['id','name','address','country','location','state'],
						where: {
							status: true,
							or: datos
						},
						include: {
							relation: 'membersCommunity',
							scope: {
								fields: ['id'],
								where: {fkUserId: idUser}
							}
					}
				}, (err, communities) => {
					if (err) { throw err; }
					console.log('Comunidades : ',communities);
					if (communities.length > 0) {
						/*
						for (var i = 0; i < communities.length; i++) {
							if (idUser.toString() === communities[i].ownerId.toString()) {
								communities[i].type = 'master';
							} else {
								communities[i].type = 'teamMember';
							}
						}*/
						next(null, communities);
					} else {
						next(err);
					}
				});
			} else {
				next(null,[]);
			}
		//});
	});
};


/**
	 * Metodo remoto que se encarga de obtener el role del usuario ??
	 *
	 * @static
	 * @param {string} idUser
	 * @param {string} idUser
	 * @param {Function} next
	 *
	 * @memberOf PgqCommunity
	 */
	static communitiesCommon(
		idUser: string,
		idUserContact: string,
		next: Function
	): void {
		let Community = app.models.PGQ_Community;
		let MembersCommunity = app.models.PGQ_MembersCommunity;
		idUser = new ObjectID(idUser);

		MembersCommunity.find({
			where: {
				status: true,
				fkUserId: idUser
			},
			include: {
				relation: 'listGroup',
				scope: {
					include: {
						relation: 'membersGroup',
						scope: {
							where: {
								fkUserId: idUserContact,
								status: true
							}
						}
					}
				}
			}
		}, (err, communitiesMembers) => {
			if (err) { next(err); }
			let datos: Array<any> = [];
			let object: any = {};
			communitiesMembers.forEach((element: any) => {
				if(element.listGroup()[0].membersGroup().length>0) {
					object = {};
					object.id = new ObjectID(element.fkCommunityId);
					datos.push(object);
				}
			});
			if(datos.length>0) {
					Community.find({
						fields: ['id','name','address','country','location','state'],
						where: {
							status: true,
							or: datos
						},
						include: {
							relation: 'membersCommunity',
							scope: {
								fields: ['id'],
								where: {fkUserId: idUser}
							}
					}
				}, (err, communities) => {
					if (err) { next(err); }
					console.log('Comunidades : ',communities);
					if (communities.length > 0) {
						next(null, communities);
					} else {
						next(err);
					}
				});
			} else {
				next(null,[]);
			}
		//});
	});
};

	/**
	 * Hook que se encarga de generar código de 9 digitos encriptado
	 *
	 * @static
	 * @param {*} ctx
	 * @param {Function} next
	 *
	 * @memberOf PgqCommunity
	 */
	static beforeSave(ctx: any, next: Function): void {
		console.log('PgqCommunity: Before Save');
		if (ctx.instance) {
			var crypto = require('crypto');
			var algorithm = 'aes-256-ctr';
			var password = 'd6F3Efeq';
			var code;
			if (!code) {
				code = Math.round(Math.random() * 99999);
			}
			//Proceso para encriptar codeQr -----Incio-----
			var cipher = crypto.createCipher(algorithm, password);
			var crypted = cipher.update(code.toString(), 'utf8', 'hex');
			crypted += cipher.final('hex');
			//Proceso para encriptar codeQr -----Fin-----
			ctx.instance.code = crypted;
			ctx.instance.codeDec = code;
		}
		next();
	}
}
module.exports = PgqCommunity;

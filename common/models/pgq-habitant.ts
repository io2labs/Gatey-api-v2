import {
		Model
} from '@mean-expert/model';

import {
	deactivateCodeQR,
	notificationContact
} from '../../server/helpers';

const ObjectID = require('mongodb').ObjectID;
const app = require('../../server/server');

/**
 * Modelo PGQ_Config
 *
 * @class PgqConfig
 */
@Model({
		hooks: {
				access: { name: 'access', type: 'operation'},
				afterSave:  { name: 'after save', type: 'operation'}
		},
		remotes: {
				deleteContact: {
				accepts: [
					{ arg: 'idHabitant', type: 'string', required: true },
				],
				returns: { root: true, type: 'boolean' },
				http: { path:'/deleteContact', verb: 'post' }
		},
		lockedContact: {
				accepts: [
					{ arg: 'idHabitant', type: 'string', required: true },
				],
				returns: { root: true, type: 'boolean' },
				http: { path:'/lockedContact', verb: 'post' }
		}
	}
})
class PgqHabitant {
	/**
	 * Creates an instance of PgqConfig.
	 *
	 * @param {*} reference
	 *
	 * @memberOf PgqConfig
	 */
		constructor(reference: any) { }

		/**
		 *
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqConfig
		 */
		static afterSave(ctx: any, next: Function): void {
				// console.log('PgqConfig: After Save');
				// var typeEvent;
				// var Audit = ctx.Model.app.models.PGQ_Audit;
				// if (ctx.isNewInstance===true){
				//     typeEvent = 'Create';
				// }else{
				//     typeEvent = 'Update';
				// }
				// if (ctx.instance) {
				//     Audit
				//     .generateAudit(ctx.Model.modelName,typeEvent,
				//                    ctx.query,
				//                    ctx.instance);
				// }
				next();
		}

				/**
		 * Bloquear contacto habitante con sus respectivos codeQR
		 *
		 * @static
		 * @param {*} idHabitant
		 * @param {Function} next
		 *
		 * @memberOf PgqHabitant

		 */
		static lockedContact(idHabitant: any, next: Function) {
			//Declaración de variables
			let Habitant: any = app.models.PGQ_Habitant;
			idHabitant = new ObjectID(idHabitant);

			console.log('Id Habitante : ',idHabitant);
			Habitant.find({
				where: {
					status: true,
					id: idHabitant
				}
			}, (err: any, habitantResult: any) => {
					if (err) {throw err;}
					let locked: boolean;
					console.log('Habitante : ',habitantResult);
					if (habitantResult.length > 0) {
							console.log('Bloqueo : ',habitantResult[0].locked);
							switch(habitantResult[0].locked) {
								case true:
									locked = false;
								break;
								case false:
									locked = true;
								break;
							}
							Habitant.updateAll({
								status: true,
								id: idHabitant
							},{
								locked: locked
							},
							(err: any, result: any) => {
								if (err) {throw err;}

								if (result.count>0) {
									//if (habitantResult[0].locked===false) {
										deactivateCodeQR(idHabitant,'H').then(resultDelete => {
											/* Generación de notificación ------DESACTIVADA TEMPORALMENTE-------
											let MembersCommunity: any = app.models.PGQ_MembersCommunity;
											MembersCommunity.find({
												where: {
													status: true,
													id: habitantResult[0].fkMembersCommunityId
												},
												include: {relation: 'community'}
											}, (err: any, membersCommunity: any) => {
													if (err) {throw err;}
													console.log('MembersCommunity Promise Notification : ',membersCommunity);
													console.log('Community Promise Notification : ',membersCommunity[0].community());
													notificationContact(
														habitantResult[0].fkUserId, membersCommunity[0].fkUserId,7,
														'Wanna add you',habitantResult[0],membersCommunity[0].community()
													).then(result => {
														if(result){
																next(null,true);
														}else{
														next(null,false);
														}
													});
											});*/
											next(null,true);
										});
									/*} else {
										next(null,true);
									}*/
								} else {
										let error = 'Failed to delete contact.';
										let errQrcode: any = new Error(error);
										errQrcode.statusCode = 400;
										next(errQrcode);
								}
							});
					} else {
							let error = 'Contact does not exist.';
							let errQrcode: any = new Error(error);
							errQrcode.statusCode = 400;
							next(errQrcode);
					}
			});
		}

		/**
		 * Eliminar contacto habitante con sus respectivos codeQR
		 *
		 * @static
		 * @param {*} idHabitant
		 * @param {Function} next
		 *
		 * @memberOf PgqHabitant

		 */
		static deleteContact(idHabitant: any, next: Function) {
			//Declaración de variables
			let Habitant: any = app.models.PGQ_Habitant;
			idHabitant = new ObjectID(idHabitant);

			Habitant.find({
				status: true,
				id: idHabitant
			}, (err: any, habitantResult: any) => {
					if (err) {throw err;}

					if (habitantResult.length > 0) {
							Habitant.updateAll({
								status: true,
								id: idHabitant
							},{
								status: false,
								locked: false
							},
							(err: any, result: any) => {
								if (err) {throw err;}

								if (result.count>0) {
									deactivateCodeQR(idHabitant,'H').then(resultDelete => {
										next(null,true);
									});
								} else {
										let error = 'Failed to delete contact.';
										let errQrcode: any = new Error(error);
										errQrcode.statusCode = 400;
										next(errQrcode);
								}
							});
					} else {
							let error = 'Contact does not exist.';
							let errQrcode: any = new Error(error);
							errQrcode.statusCode = 400;
							next(errQrcode);
					}
			});
		}

		/**
		 *
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqConfig
		 */
		static access(ctx: any, next: Function): void {
				console.log('PgqConfig: Access');
				// se debe revisar este codigo ya que guarda muchos registros
				// var Audit = ctx.Model.app.models.PGQ_Audit;
				// Audit.generateAudit(ctx.Model.modelName,'Find',ctx.query);
				next();
		}
}
module.exports = PgqHabitant;

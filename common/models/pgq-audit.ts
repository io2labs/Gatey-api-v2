import {
	Model
} from '@mean-expert/model';
const app = require('../../server/server');
/**
 * Modelo PGQ_Audit
 *
 * @class PgqAudit
 */
@Model({
	hooks: {},
	remotes: {
		generateAudit: {
			accepts: [
				{ arg: 'collections', type: 'string' },
				{ arg: 'eventHea', type: 'string' },
				{ arg: 'detailEvent', type: 'string' },
				{ arg: 'detailEventObject', type: 'json' },
				{ arg: 'detailEventObjectErr', type: 'json' }
			],
			returns: { arg: 'success', type: 'boolean' },
			http: { path: '/generateAudit', verb: 'post' }
		}
	}
})
class PgqAudit {
	/**
	 * Creates an instance of PgqAudit.
	 *
	 * @param {*} reference
	 *
	 * @memberOf PgqAudit
	 */
	constructor(reference: any) {
	}

	/**
	 * Genera la auditoria
	 *
	 * @static
	 * @param {string} collections
	 * @param {string} eventHea
	 * @param {string} detailEvent
	 * @param {Object} detailEventObject
	 * @param {Object} detailEventObjectErr
	 * @param {Function} next
	 *
	 * @memberOf PgqAudit
	 */
	static generateAudit(
		collections: string,
		eventHea: string,
		detailEvent: string,
		detailEventObject: Object,
		detailEventObjectErr: Object,
		next: Function ): void {
		next(null, true);
		let Audit = app.models.PGQ_Audit;

		Audit.create({
			host: 'localhost',
			collections: collections,
			eventHea: eventHea,
			detailEvent: detailEvent,
			detailEventObject: detailEventObject,
			created: Date(),
			fkUserId: '',
			accessTocken: ''
		}, (err, result) => {
			if (err) { throw err; }
			// console.log(result);
		});
	};
}
module.exports = PgqAudit;

import {
    Model
} from '@mean-expert/model';

import {
    getCommunity,
		getUserCommunity,
		deleteGroupContact,
		getGroupUserDefault,
    notificationContact,
		getUserId,updateOrCreateContact
} from '../../server/helpers';

const app = require('../../server/server');
const ObjectID = require('mongodb').ObjectID;

/**
* @class Modelo PGQ_Members
*
**/
@Model({
    hooks: {
        afterSave: { name: 'after save', type: 'operation' },
        access: { name: 'access', type: 'operation' }
    },
    remotes: {
        addContact: {
            accepts:[
                { arg: 'idCommunity', type: 'string' },
                { arg: 'idUser', type: 'string' },
                { arg: 'idUserContact', type: 'string' }
            ],
            returns:{ arg: 'success', type: 'boolean' },
            http:{ path:'/addContact', verb: 'post' }
        },
        deleteContact: {
             accepts:[
                { arg: 'idCommunity', type: 'string' },
                { arg: 'idUser', type: 'string' },
                { arg: 'idUserContact', type: 'string' }
            ],
            returns:{ arg: 'success', type: 'boolean' },
            http:{ path:'/deleteContact', verb: 'post' }
        },
        listContact: {
            accepts:[
                { arg: 'idUser', type: 'string' }],
            returns:{ arg: 'success', type: 'json' },
            http:{ path:'/listContact', verb: 'post' }
        }
    }
})

class PgqMembers {
		/**
		 * Creates an instance of PgqMembers.
		 *
		 * @param {*} reference
		 *
		 * @memberOf PgqMembers
		 */
    constructor(reference: any) { }

		/**
		 * Metodo remoto para eliminar contactos
		 *
		 * @static
		 * @param {string} idGroup
		 * @param {string} idUserContact
		 * @param {string} idUser
		 * @param {Function} next
		 *
		 * @memberOf PgqMembers
		 */
    static deleteContact(
        idCommunity:string,
        idUser: string,
        idUserContact: string,
        next: Function ): void {
        //Declaración de variables generales
        //idGroup = new ObjectID(idGroup);
        idUserContact = new ObjectID(idUserContact);
        //let Members: any = app.models.PGQ_Members;
        getGroupUserDefault(idUser,idCommunity).then(idGroup => {
            deleteGroupContact(idUserContact,idGroup).then(resultGroup1 => {
            console.log('delete group 1 : ', resultGroup1);
            if (resultGroup1){
                getUserCommunity(idGroup).then(membersCommunity => {
                    console.log('Comunidad => Promise 1 : ',membersCommunity[0].fkCommunityId);
                    console.log('User => Promise 1 : ',membersCommunity[0].fkUserId);
                    getGroupUserDefault(idUserContact,membersCommunity[0].fkCommunityId).then(fkGroupId => {
                        console.log('Grupo 2 : ',fkGroupId);
                        if (fkGroupId){
                            deleteGroupContact(membersCommunity[0].fkUserId,fkGroupId).then(resultGroup2 => {
                                console.log('delete group 2 : ', resultGroup2);
                                next(null,true);
                            });
                        }
                    });
                });
            } else {
                    //Manejo de código de errores
                    var error = 'Contact could not be deleted.';
                    var errQrcode: any = new Error(error);
                    errQrcode.statusCode = 400;
                    next(null,false);
            }
        });
    }, (err: any) => {
        next(err);
    });
  };

		/**
		 * Metodo remoto para añadir contactos
		 *
		 * @static
		 * @param {string} idGroup
		 * @param {string} idUser
		 * @param {string} idUserContact
		 * @param {Function} next
		 *
		 * @memberOf PgqMembers
		 */
    static addContact (
			idCommunity: string,
            idUser: string,
			idUserContact: string,
      next: Function ): void {

        //idGroup = new ObjectID(idGroup);
        idCommunity = new ObjectID(idCommunity);
        idUser = new ObjectID(idUser);
        idUserContact = new ObjectID(idUserContact);
        //--------------Crea Contacto----------------
        getGroupUserDefault(idUser,idCommunity).then(idGroup => {
            updateOrCreateContact(idUserContact,idGroup,1).then(members => {
                //--------------Generar notificación----------------
                getCommunity(idGroup).then(communityObject => {
                    getUserId(idGroup).then(userId =>  {
                        notificationContact(
                            userId,idUserContact,1,
                            'Wanna add you',members,communityObject[0]
                        ).then(result => {
                            if(result){
                                    next(null,true);
                            }else{
                            next(null,false);
                            }
                        });
                    });
                });
                //--------------Generar notificación----------------
            });
        },(err: any) => {
            next(err);
        })
        //--------------Crea Contacto----------------
    }

		/**
		 * Metodo remoto para obtener contactos
		 *
		 * @static
		 * @param {string} idUser
		 * @param {Function} next
		 *
		 * @memberOf PgqMembers
		 */
    static listContact (
        idUser: string,
        next: Function ): void {
        //Declaración de variables generales
        idUser = new ObjectID(idUser);
        //Variables para auditoria a nivel general
        //var collections = 'PGQ_Members';
        //var typeEvent  = 'Find';
        //var numErr = 400;
        var msgErr = 'Contact not added.';
        //var detailEvent = 'where : {status: true, fkGroupUserId: }';
        //var Audit = app.models.PGQ_Audit;
        //var Config = app.models.PGQ_Config;
        let Group = app.models.PGQ_GroupUser;
        Group.find({
            where: {
                status: true,
                fkUserId: idUser
            }
        },(err,group)=> {
            if (err) {throw err;}
            if (group.length > 0){
                //detailEvent = 'where : {status: true, fkGroupUserId: '+group[0].id+'}';
                let Members = app.models.PGQ_Members;
                Members.find({
                    where: {
                        status: true,
                        fkGroupUserId: group[0].id
                    }
                },(err,members)=> {
                    if (err) {throw err;}
                    if (members.length > 0){
                        var datos = [];
                        for (var i = 0; i < members.length; i++) {
                            var object: any = {}
                            object._id = new ObjectID(members[i].fkUserId);
                            datos.push(object);
                        }
                        var User = app.models.PGQ_User;
                        User.find({
                            where: {
                                or: datos
                            }
                        },(err,listUser)=> {
                            if (err) {throw err;}
                            //detailEvent = 'where : {or: '+datos+'}';
                            var countListUser = listUser.length;
                            if(countListUser>0){
                                //Audit.generateAudit(Collections, typeEvent, detailEvent, listUser);
                                next(null,listUser);
                            }
                        });
                    } else {
                        // detailEvent = '';
                        // Config.jsonError(numErr,msgErr).then(json => {
                        //     Audit.generateAudit(collections, typeEvent, detailEvent, json);
                        // });
                        var errQrcode: any = new Error(msgErr);
                        errQrcode.statusCode = 400;
                        next(errQrcode,false);
                    }
                });
            } else {
                msgErr = 'User does not have assigned group.';
                /*Config
                .jsonError(numErr,msgErr).then(json => {
                Audit
                .generateAudit(
                    collections, typeEvent, detailEvent, json
                );
                });*/
                var errQrcode: any = new Error(msgErr);
                errQrcode.statusCode = 400;
                next(errQrcode,false);
            }
        });
    }

		/**
		 *
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqMembers
		 */
    static afterSave(ctx: any, next: Function): void {
        //console.log('PgqMembers: After Save');
        // var typeEvent;
        // var Audit = ctx.Model.app.models.PGQ_Audit;
        // if (ctx.isNewInstance===true){
        //     typeEvent = 'Create';
        // }else{
        //     typeEvent = 'Update';
        // }
        // if (ctx.instance) {
        //     Audit.generateAudit(ctx.Model.modelName,typeEvent, ctx.query, ctx.instance);
        // }
        next();
    }

		/**
		 *
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqMembers
		 */
    static access(ctx: any, next: Function): void {
        //console.log('PgqMembers: Access');
        // var Audit = ctx.Model.app.models.PGQ_Audit;
        // Audit.generateAudit(ctx.Model.modelName,'Find',ctx.query);
        next();
    }
}
module.exports = PgqMembers;

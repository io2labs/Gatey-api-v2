import {
    Model
} from '@mean-expert/model';

/**
 * Modelo PGQ_Msg
 *
 * @class PgqMsg
 */
@Model({
    hooks: {
        afterSave: { name: 'after save', type: 'operation'},
        access:{ name: 'access', type: 'operation'}
    },
    remotes: {}
})
class PgqMsg {
		/**
		 * Creates an instance of PgqMsg.
		 *
		 * @param {*} reference
		 *
		 * @memberOf PgqMsg
		 */
    constructor(reference: any) {  }

    /**
     *
     *
     * @static
     * @param {*} ctx
     * @param {Function} next
     *
     * @memberOf PgqMsg
     */
		static afterSave(ctx: any, next: Function): void {
        //console.log('PgqMsg: After Save');
        // var typeEvent;
        // var Audit = ctx.Model.app.models.PGQ_Audit;
        // if (ctx.isNewInstance===true){
        //     typeEvent = 'Create';
        // }else{
        //     typeEvent = 'Update';
        // }
        // if (ctx.instance) {
        //     Audit.generateAudit(ctx.Model.modelName,typeEvent, ctx.query, ctx.instance);
        // }
        next();
    }

		/**
		 *
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqMsg
		 */
    static access(ctx: any, next: Function): void {
        //console.log('PgqMsg: Access');
        // var Audit = ctx.Model.app.models.PGQ_Audit;
        // Audit.generateAudit(ctx.Model.modelName,'Find',ctx.query);
        next();
    }
}
module.exports = PgqMsg;

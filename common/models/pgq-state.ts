import {
    Model
} from '@mean-expert/model';

/**
 * Modelo PGQ_State
 *
 * @class Pgqstate
 */
@Model({
    hooks: {},
    remotes: {}
})
class Pgqstate {
		/**
		 * Creates an instance of Pgqstate.
		 *
		 * @param {*} reference
		 *
		 * @memberOf Pgqstate
		 */
    constructor(reference: any) {
    }
}
module.exports = Pgqstate;

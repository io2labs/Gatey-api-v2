import {
    Model
} from '@mean-expert/model';

import {
	createGroupMembersDefault
} from '../../server/helpers';

const app = require('../../server/server');
const ObjectID = require('mongodb').ObjectID;

/**
 * Modelo PGQ_Memberscommunity
 *
 * @class PgqMemberscommunity
 */
@Model({
    hooks: {
			afterSave: { name: 'after save', type: 'operation' }
		},
    remotes: {
        listContact: {
            accepts: [
                { arg: 'idCommunity', type: 'string' },
                { arg: 'idUser', type: 'string'}
            ],
            returns:{ root: true, type: 'array' },
            http:{ path: '/listContact', verb: 'post' }
        },
				listContactAll: {
            accepts: [
                { arg: 'idUser', type: 'string'}
            ],
            returns:{ root: true, type: 'array' },
            http:{ path: '/listContactAll', verb: 'post' }
        }
    }
})
class PgqMemberscommunity {
		/**
		 * Creates an instance of PgqMemberscommunity.
		 *
		 * @param {*} reference
		 *
		 * @memberOf PgqMemberscommunity
		 */
    constructor(reference: any) {  }

		static afterSave(ctx: any, next: Function): void {
			if (ctx.isNewInstance===true) {
				console.log('Instance : ',ctx.instance);
				createGroupMembersDefault(ctx.instance).then(createDefault => {
					if(createDefault) {
						next();
					}
				});
			} else {
				next();
			}
    }

		/**
		 * Metodo remoto que se encarga de listar los contactos de
		 * una comunidad en especifico
		 *
		 * @static
		 * @param {string} idCommunity
		 * @param {string} idUser
		 * @param {Function} next
		 *
		 * @memberOf PgqMemberscommunity
		 */
    static listContact(
        idCommunity: string,
        idUser: string,
        next: Function ): void {

        let MembersCommunity: any = app.models.PGQ_MembersCommunity;
        MembersCommunity.find({
            where: {
                fkCommunityId: idCommunity,
                status: true,
								check: false,
                fkUserId: idUser
            }
        }, (err, membersCommunity) => {
            if (err) { throw err; }
            console.log('MembersCommunity  aqui: ',membersCommunity);

						if (membersCommunity.length > 0) {
							let GroupUser: any = app.models.PGQ_GroupUser;
							GroupUser.find({
									where:{
											fkMembersCommunityId: membersCommunity[0].id,
											name: 'default',
											status: true
									},
									include:{
											relation: 'membersGroup',
											scope: {
													where:{
															fkUserId:{
																	neq: idUser
															},
															status: true
													},
													include: {
															relation: 'user'
													}
											},
									},
							}, (err, listGroup) => {
									if (err) { throw err; }
									console.log('listGroup : ',listGroup);
									if (listGroup.length > 0) {
										let Members: any = app.models.PGQ_Members;
										Members.find({
												fields: ['fkUserId'],
												where:{
														fkGroupUserId: listGroup[0].id,
														or: [
																	{ and: [{ status: true }, { check: false }] },
																	{ and: [{ status: false }, { check: true }] }
														]
												},
										}, (err, listMembers) => {
												if (err) { throw err; }
												//console.log('listMembers : ',listMembers);
												//Construir Array para excluir usuarios de la lista de la comunidad
												let datos = [];
												for (var i = 0; i < listMembers.length; i++) {
														datos[i] = new ObjectID(listMembers[i].fkUserId);
												}
												datos.push(idUser);
												console.log('Datos : ',datos);
												//Añadir el usuario logueado
												//Construir Array para excluir usuarios de la lista de la comunidad
												MembersCommunity.find({
														where:{
																fkCommunityId: idCommunity,
																status: true,
																fkUserId: { nin: datos }
														},include:{
																relation: 'user'
														}
												}, (err, membersCommunity) => {
														if (err) { throw err; }
														//console.log('membersCommunity : ',membersCommunity);
														var arrayGroup = [];
														listGroup[0].userMembersCommunity = membersCommunity;
														arrayGroup.push(listGroup[0]);
														//cb(null, listGroup[0]);
														next(null, arrayGroup);
												});
										});
										} else {
											next(null, []);
										}
										//console.dir(listMembers);
										// COMENTADO TEMPORALMENTE
										// this.listCommunityGroup(listGroup[0].id, idCommunity).then(
										//     userCommunity => {
										//         //console.log('Members Community : ',membersCommunity);
										//         //console.log(listGroup[0]);
										//         //var object = new Object({});
										//         communityMembers = userCommunity;
										//         listGroup[0].userCommunity = communityMembers;
										//         console.log('Prueba : ', userCommunity);
										//     });
										//listGroup[0].userCommunity = communityMembers;
								});
						} else {
							next(null, []);
						}
        });
    };

		/**
		 * Metodo remoto que se encarga de listar todos los contactos (todas las comunidades)
		 *  de un usuario en especifico
		 *
		 * @static
		 * @param {string} idUser
		 * @param {Function} next
		 *
		 * @memberOf PgqMemberscommunity
		 */
		static listContactAll(
		idUser: string,
		next: Function ): void {

        let MembersCommunity: any = app.models.PGQ_MembersCommunity;
        MembersCommunity.find({
            where: {
                status: true,
								check: false,
                fkUserId: idUser
            }
        }, (err, membersCommunity) => {
						if (err) { next(err); }
            console.log('MembersCommunity  aqui: ',membersCommunity);

						if (membersCommunity.length > 0) {
							//Inicio-Creación de lista de grupos a partir de fkMembersCommunityId
								let dataMembersCommunity = [];
								for (let i = 0; i < membersCommunity.length; i++) {
									dataMembersCommunity[i] = new ObjectID(membersCommunity[i].id);
								}
								console.log('Data MembersCommunity : ',dataMembersCommunity);
							//Fin-Creación de lista de grupos a partir de fkMembersCommunityId
							let GroupUser: any = app.models.PGQ_GroupUser;
							GroupUser.find({
									where:{
											fkMembersCommunityId: {
														in: dataMembersCommunity
												},
											name: 'default',
											status: true
									},
									include:{
											relation: 'membersGroup',
											scope: {
													where:{
															fkUserId:{
																	neq: idUser
															},
															status: true
													},
													include: {
															relation: 'user'
													}
											},
									},
							}, (err, listGroup) => {
									if (err) { next(err); }
									//Inicio-Creación de lista de grupos a partir de fkMembersCommunityId
										let dataGroup = [];
										for (let i = 0; i < listGroup.length; i++) {
											dataGroup[i] = new ObjectID(listGroup[i].id);
										}
										console.log('Data Group : ',listGroup);
									//Fin-Creación de lista de grupos a partir de fkMembersCommunityId
									console.log('listGroup : ',listGroup);
									if (listGroup.length > 0) {
										let Members: any = app.models.PGQ_Members;
										Members.find({
												where:{
														fkGroupUserId:  { in: dataGroup },
														or: [
																	{ and: [{ status: true }, { check: false }] },
																	{ and: [{ status: false }, { check: true }] }
														]
												},
													include:{
																relation: 'user'
														}
										}, (err, listMembers) => {
												if (err) { next(err); }
												console.log('listMembers : ',listMembers);
												next(null, listMembers);
												/*
												//Construir Array para excluir usuarios de la lista de la comunidad
												let datos = [];
												for (var i = 0; i < listMembers.length; i++) {
														datos[i] = new ObjectID(listMembers[i].fkUserId);
												}
												//datos.push(idUser);
												console.log('Datos : ',datos);
												//Añadir el usuario logueado
												//Construir Array para excluir usuarios de la lista de la comunidad
												MembersCommunity.find({
														where:{
																status: true,
																fkUserId: { in: datos }
														},include:{
																relation: 'user'
														}
												}, (err, membersCommunity) => {
														if (err) { next(err); }
														let arrayGroup = [];
														listGroup[0].userMembersCommunity = membersCommunity;
														arrayGroup.push(listGroup[0]);
														next(null, arrayGroup);
												});
												*/
										});
										} else {
											next(null, []);
										}
								});
						} else {
							next(null, []);
						}
        });
    };

}
module.exports = PgqMemberscommunity;

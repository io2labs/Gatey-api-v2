import {
    Model
} from '@mean-expert/model';

import {
	getCommunity,
	notificationContact,
	getGroupUserDefault,
	updateOrCreateContact,
	getInvited,
	getCheckUser,
	createUserTypeCommunity,
	updateInvitation
} from '../../server/helpers';
//Declaración de constantes
const app = require('../../server/server');
const ObjectID = require('mongodb').ObjectID;

/**
 * Modelo PGQ_Notification
 *
 * @class PgqNotification
 */
@Model({
    hooks: {
			//afterSave:  { name: 'after save', type: 'operation' },
			beforeSave: { name: 'before save', type: 'operation' },
		},
    remotes: {
        getNotification: {
            accepts: [
                { arg: 'idUser', type: 'string' },
            ],
            returns: { root: true, type: 'boolean'},
            http: { path:'/getNotification', verb: 'post' }
        },
		checkNotification: {
			accepts: [
				{ arg: 'idNotification', type: 'string', required: true },
				{ arg: 'check', type: 'boolean', required: true, default: false },
			],
			returns: { root: true, type: 'boolean' },
			http: { path:'/checkNotification', verb: 'post' }
		},
		checkAllViewNotification: {
			accepts: [
				{ arg: 'idUser', type: 'string', required: true },
			],
			returns: { root: true, type: 'boolean' },
			http: { path:'/checkAllViewNotification', verb: 'post' }
		}
    }
})

class PgqNotification {
		/**
		 * Creates an instance of PgqNotification.
		 *
		 * @param {*} reference
		 *
		 * @memberOf PgqNotification
		 */
    constructor(reference: any) {  }

		/**
		 * Hook que se encarga de agregar
		 * notificaciones de recepción antes de modificarlas
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqNotification
		 */
		static beforeSave(
			ctx: any,
			 next: Function
		): void {
			//console.log('beforeSave : ', ctx.isNewInstance);
			/*if (ctx.isNewInstance===undefined) {
				console.log(ctx.where);
				next();
			} else {
				next();
			}*/
			//Declaración de variables
			let Notification: any = app.models.PGQ_Notification;
			let type: number;
			let typeDetail: string;

			if (ctx.isNewInstance===undefined && ctx.where._id) {
				Notification.find({
					where: {
						_id: ctx.where._id,
						status: true,
						check: true
					}
				}, (err: any, userNotification: any) => {
					if (err) {throw err;}
					console.log('afterSave : ',userNotification);
					let notificationUser: any;
					notificationUser = userNotification[0];
					if (userNotification.length===1 &&
							(notificationUser.type===1 || notificationUser.type===3 ||
							 notificationUser.type===5 || notificationUser.type===7
							)
						) {
						switch (notificationUser.type) {
							case 1:
								type = 2;
								typeDetail = 'Has accepted your friendship request';
								break;
							case 3:
								type = 4;
								typeDetail = 'CodeQr seen';
								break;
							case 5:
								type = 6;
								typeDetail = 'Has accepted your friendship request';
								break;
							case 7:
								type = 8;
								typeDetail = 'Confirmed codeQR generation';
								break;
						}
						//--------------Generar notificación----------------
							notificationContact(
								notificationUser.fkUserId,notificationUser.fkUserIdSend,
								type,typeDetail,
								{},notificationUser.community
							).then(result => {
								if(result){
										next();
								}else{
									next();
								}
							});
						//--------------Generar notificación----------------
					} else {
						next();
					}
				});
			} else if(ctx.isNewInstance===undefined && ctx.where.type){
					Notification.find({
						where: {
							fkUserId: ctx.where.fkUserId,
							status: true,
							check: true,
							type: ctx.where.type.inq[1]
						}
					}, (err: any, userNotification: any) => {
						if (err) {throw err;}
						console.log('Notificaciones de tipo 3 : ',userNotification);
						console.log('Where : ', ctx.where);

						next();
					});
			} else {
				console.log('Insert');
				next();
			}
    }

		/**
		 * Hook que se encarga de agregar notificación de recepción
		 *
		 * @static
		 * @param {*} ctx
		 * @param {Function} next
		 *
		 * @memberOf PgqNotification
		 */
		/*
		static afterSave(ctx: any, next: Function): void {
			//Declaración de variables
			let Notification: any = app.models.PGQ_Notification;
			let type: number;
			let typeDetail: string;

			if (ctx.isNewInstance===undefined && ctx.where._id) {
				Notification.find({
					where: {
						_id: ctx.where._id,
						status: true,
						check: true
					}
				}, (err: any, userNotification: any) => {
					if (err) {throw err;}
					console.log('afterSave : ',userNotification);
					let notificationUser: any;
					notificationUser = userNotification[0];
					if (userNotification.length===1 &&
						 (notificationUser.type===1 || notificationUser.type===3)){
						switch (notificationUser.type) {
							case 1:
								type = 2;
								typeDetail = 'I accept you as contact';
								break;
							case 3:
								type = 4;
								typeDetail = 'CodeQr seen';
								break;
						}
						//--------------Generar notificación----------------
							notificationContact(
								notificationUser.fkUserId,notificationUser.fkUserIdSend,
								type,typeDetail,
								{},notificationUser.community
							).then(result => {
								if(result){
										next();
								}else{
								next();
								}
							});
						//--------------Generar notificación----------------
					} else {
						next();
					}
				});
			}else{
				console.log('Insert');
				next();
			}
    }
*/
		/**
		 * Método remote encargado de chequear todas las notificaciones de tipo 2,3 y 4 (view)
		 *
		 * @static
		 * @param {string} idUser
		 * @param {Function} next
		 *
		 * @memberOf PgqNotification
		 */
		static checkAllViewNotification(idUser: any, next: Function) {
			//Declaración de variables
			let Notification: any = app.models.PGQ_Notification;
			let datos: Array<number> = [ 2, 3, 4, 6, 8 ];
			idUser = new ObjectID(idUser);

			Notification.updateAll({
				status: true,
				check: true,
				fkUserId: idUser,
				type: { inq: datos }
			},{
				status: false,
				check: false
			},
			(err: any, result: any) => {
				if (err) {throw err;}

					next(null,result.count);
			});
		}

		/**
		 * Metodo remoto que se encarga de chequear la notificación :
		 *
		 * @static
		 * @param {string} idNotification
		 * @param {Function} next
		 *
		 * @memberOf PgqNotification
		 */
		static checkNotification(idNotification: string, check: boolean, next: Function) {
				//Declaración de variables generales
				idNotification = new ObjectID(idNotification);
				let Notification: any = app.models.PGQ_Notification;
				let Members: any = app.models.PGQ_Members;

				Notification.find({
					where: {
						_id: idNotification,
						check: true,
						status: true
					}
				}, (err, userNotification) => {
						if (err) {next(err);}
						console.log('Resultado ....... : ',userNotification);

						if (userNotification.length > 0 && (userNotification[0].type===1 || userNotification[0].type===5 || userNotification[0].type===7)) {

								//Actualiza Notificación
								Notification.updateAll({
										status: true,
										check: true,
										_id: idNotification
								},{
										status: false,
										check: false
								}, (err, updateNotification) => {
										if (err) { next(err); }
										console.log('Count : ',updateNotification);
										if(updateNotification.count > 0) {
												if (check) {
													//console.log('Check - if : ',check);
													//Activamos el usuario
													console.log('idMembers : ',userNotification[0].detail.id);
													console.log('Type .... : ',userNotification[0].type);
													if(userNotification[0].type===1) {
														console.log('IF');
														Members.updateAll({
															_id: userNotification[0].detail.id,
															status: false,
															check: true
														},{
															status: true,
															check: false,
															description: {name: 'default',status: false}
														},(err, membersUpdate) => {
															if (err) {next(err);}

															console.log('Update count : ',membersUpdate);

															if (membersUpdate.count>0) {
																//Añadir usuario en el otro grupo
																console.log(userNotification[0].fkUserId,
																	userNotification[0].community.id);
																getGroupUserDefault(
																	userNotification[0].fkUserId,
																	userNotification[0].community.id
																).then(idGroup => {
																	updateOrCreateContact(userNotification[0].fkUserIdSend,idGroup,2)
																		.then(membersObject => {
																			next(null, true);
																		});
																	/*
																	Members.create({
																		fkGroupUserId: idGroup,
																		fkUserId: userNotification[0].fkUserIdSend,
																		created: Date(),
																		status: true,
																		check: false
																	},(err, result) => {
																			if (err) {throw err;}
																			if(result) {
																				next(null, true);
																			} else {
																				//Ojo analizar este caso
																				next(null, false);
																			}
																	});*/
																}, (err) => {
																		next(err, false);
																});
															} else {
																next(null, false);
															}
														});
													} else if (userNotification[0].type===5) {
															getInvited(userNotification[0].detail.code,
																				userNotification[0].detail.email).then(objectInvited => {
																getCheckUser(userNotification[0].detail.email).then(objectUser => {
																	createUserTypeCommunity(objectInvited[0],
																													objectUser[0],true,false).then(result => {
																		console.log('result : ',result);
																		console.log('----------------------j--------------');
																		if(result) {
																			updateInvitation(objectInvited[0].id).then(updateInvited => {
																				console.log('----------------------k--------------');
																				console.log('updateInvitation : ',updateInvited);
																				if(updateInvited) {
																					console.log('----------------------d--------------');
																					console.log('Añadida Notificación 6 invitación : ',objectInvited);
																			    console.log('Result : ',result);
																			    next(null, true);
																				}
																			});
																		}
																	});
																});
															});
													} else if(userNotification[0].type===7) {
														console.log('7');
														let CodeQR: any = app.models.PGQ_QRCode;
														console.log('Detalle notificación : ',userNotification[0].detail);
														CodeQR.generateQrCode(userNotification[0].detail.id,
																			userNotification[0].detail.deliveryDate,
																			userNotification[0].detail.graceTime,
															   (err, codeQR) => {
																   if (err) {next(err);}

																   if(codeQR) {
																	   next(null, true);
																   } else {
																	   let error = 'CodeQR not generated.';
																	   let errQrcode: any = new Error(error);
																	   errQrcode.statusCode = 400;
																	   next(errQrcode,false);
																   }
														});
													}
												} else {
															console.log('ELSE');
															next(null, false);
												}
										} else {
												next(null, false);
										}
								});
							} else {
									//Manejo de código de errores
									var error = 'The notification does not exist.';
									var errQrcode: any = new Error(error);
									errQrcode.statusCode = 400;
									next(errQrcode,false);
							}
				});
			};

    /**
     * Metodo remoto para obtener las notificaciones activas por usuario :
     *
     * @static
     * @param {string} idUser
     * @param {Function} next
     *
     * @memberOf PgqNotification
     */
    static getNotification(idUser: string, next: Function) {
			//Declaración de variables generales
			idUser = new ObjectID(idUser);

			let Notification = app.models.PGQ_Notification;
			Notification.find({
					where: {
							and: [
									{fkUserId: idUser},
									{check: true},
									{status: true}
							]
					},
					include: [
							{ relation: 'userRecipient' }
					]
			}, (err, result) => {
					if (err) {throw err;}
					console.log(result);
					next(null,result);
			});
	};
}
module.exports = PgqNotification;

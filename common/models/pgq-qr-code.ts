import {
    Model
} from '@mean-expert/model';

import {
    promiseQR,
		getCommunity,
    notificationContact,
		getUserId,getMembersAutoGenerated,
		validUserComunityCodeQr, createCodeQr,
		getGroupUserDefault, getMembers,
		getMembersCodeQr,getConfigCommunity,
		codeDecrypt,scanCodeQr
} from '../../server/helpers';

const app = require('../../server/server');
const ObjectID = require('mongodb').ObjectID;
const crypto = require('crypto');

/**
 * Modelo PGQ_QRCode
 *
 * @class PgqQrCode
 */
@Model({
    hooks: {
        //beforeSave: { name: 'before save', type: 'operation' },
        //afterSave:  { name: 'after save', type: 'operation' }
        //afterRemote:{ name: 'create', type: 'afterRemote' }
    },
    remotes: {
        reqQrCode: {
            accepts: [
                { arg: 'idUser', type: 'string'}
            ],
            returns: { root: true, type: 'json' },
            http: { path:'/reqQrCode', verb: 'post' }
        },
        scanCodeQr: {
            accepts: [
                { arg: 'idCodeQr', type: 'string'}
            ],
            returns: { arg: 'success', type: 'boolean' },
            http: { path:'/scanCodeQr', verb: 'post' }
        },
        selfGenerated: {
            accepts: [
                {arg: 'idUser', type: 'string'},
								{arg: 'idCommunity', type: 'string'},
            ],
            returns: {root: true, type: 'boolean'},
            http: {path:'/selfGenerated', verb: 'post'}
        },
				generateQrCode: {
            accepts: [
                {arg: 'idUser', type: 'string'},
								{arg: 'idUserContact', type: 'string'},
								{arg: 'idCommunity', type: 'string'},
            ],
            returns: {root: true, type: 'json'},
            http: {path:'/generateQrCode', verb: 'post'}
        }
    }
})

class PgqQrCode {
		/**
		 * Creates an instance of PgqQrCode.
		 *
		 * @param {*} reference
		 *
		 * @memberOf PgqQrCode
		 */
    constructor(reference: any) {  }
		/**
		 * listado con los codeQR por usuario y comunidad
		 *
		 * @static
		 * @param {*} idUser
		 * @param {Function} next
		 *
		 * @memberOf PgqQrCode
		 */
			static reqQrCode(
				idUser: string,
				next: Function
			) : void {
				getMembersCodeQr(idUser).then(codeQrActive => {
					next(null,codeQrActive);
				}).catch(err => {
					next(err);
				});
			};

		/**
		 * Scanear codeQR (Chequeo de entrada y salida)
		 *
		 * @static
		 * @param {string} idCodeQr
		 * @param {Function} next
		 *
		 * @memberOf PgqQrCode
		 */
		/*
    static scanCodeQr(idCodeQr: string, next: Function) {
        //Desencriptar codeQr -----Inicio-----
        let crypto = require('crypto');
        let algorithm = 'aes-256-ctr';
        let password = 'd6F3Efeq';

        let decipher = crypto.createDecipher(algorithm,password)
        let codeQrDec = decipher.update(idCodeQr,'hex','utf8')
        codeQrDec += decipher.final('utf8');
        //Desencriptar codeQr -----Fin-----

        let QRCode = app.models.PGQ_QRCode;
        QRCode.find({
            where: {
                status: true,
                id: codeQrDec
            }
        }, (err,result) => {
            if (err) throw err;
            //console.log(result);
            if(result.length>0){

                //ESTE BLOQUE DE CÓGIDO QUE SE PODRA UNIFICAR(ANALISIS) con
                //METODO REMOTO #1 O CREAR UN METODO REMOTO #2 O #3
                //NOTA: PARA PROXIMO SPRINT.
                //-------------------------FIN---------------------

                //Fecha Actual
                var fechaActual = new Date();
                var horaActual = fechaActual.getHours();
                var minutosActual = fechaActual.getMinutes();
                var segundosActual = fechaActual.getSeconds();
                var yearActual = fechaActual.getFullYear();
                var monthActual = fechaActual.getMonth();
                var dayActual = fechaActual.getDay();

                //Fecha del codeQr
                var fechaCodeQr = result[0].deliveryDate;
                var horaCodeQr = fechaCodeQr.getHours();
                var minutosCodeQr = fechaCodeQr.getMinutes();
                var yearCodeQr = fechaCodeQr.getFullYear();
                var monthCodeQr = fechaCodeQr.getMonth(fechaCodeQr);
                var dayCodeQr = fechaCodeQr.getDay(fechaCodeQr);

                //Fecha Expiración codeQr
                var fechaExpCodeQr = result[0].timeExpire;
                var horaCodeQrExp = fechaExpCodeQr.getHours();
                var minutosCodeQrExp = fechaExpCodeQr.getMinutes();

                // asignar el valor de las unidades en milisegundos
                var msecPerMinute = 1000 * 60;
                var msecPerHour = msecPerMinute * 60;
                var msecPerDay = msecPerHour * 24;

                // asignar la fecha en milisegundos
                var dateIniMsec = fechaCodeQr.getTime();
                var dateExpMsec = fechaExpCodeQr.getTime();
                var dateActualMsec = fechaActual.getTime();

                // Obtener la diferencia en milisegundos del codeQr
                var intervalCodeQr = (dateIniMsec - dateExpMsec)*(-1);
                var intervalActual = (dateIniMsec - dateActualMsec)*(-1);
                var intervalActive = dateIniMsec - dateActualMsec;

                if (intervalActive > 0) {
                    next(null,false);
                } else {
                    if ((intervalCodeQr > intervalActual) &&
                    (result[0].statusIn === false &&
                    result[0].statusOut === false)) {
                        QRCode.updateAll({
                            status: true, id: codeQrDec,
                            statusIn: false, statusOut: false
                        },{
                            statusIn: true
                        },(err, result) => {
                            if (err) throw err;
                            //console.log(result.count);
                            if(result.count > 0){
                                next(null,true);
                            } else {
                                next(null,false);
                            }
                            //console.log(result);
                        });
                    } else {
                        if (result[0].statusIn === false && result[0].statusOut === false){
                            QRCode.updateAll({
                                status: true,id: result[0].id,
                                statusIn: false, statusOut: false
                            },{
                                status: false
                            }, (err,result) => {
                                if (err) throw err;
                                next(null,false);
                            });
                        } else {
                            QRCode.find({
                                where: {
                                    status: true, id: codeQrDec,
                                    statusIn: true, statusOut: false
                                }
                            },(err, result) => {
                                if (err) throw err;
                                //console.log(result);
                                if(result.length > 0) {
                                    QRCode.updateAll({
                                        status: true,id: codeQrDec,
                                        statusIn: true,statusOut: false
                                    },{
                                        statusOut: true,
                                        status: false
                                    }, (err, result) => {
                                        if (err) throw err;
                                        //console.log(result.count);
                                        if(result.count > 0){
                                        //Borrar imagen falta
                                            next(null,true);
                                        } else {
                                            next(null,false);
                                        }
                                        //console.log(result);
                                    });
                                } else {
                                    next(null,false);
                                }
                            });
                        }
                    }
                }

                //ESTE BLOQUE DE CÓGIDO QUE SE PODRA UNIFICAR(ANALISIS) con
                //METODO REMOTO #1 O CREAR UN METODO REMOTO #2 O #3
                //NOTA: PARA PROXIMO SPRINT.
                //-------------------------FIN---------------------

            } else {
                next(null,false);
            }
        });
    }
*/

		/**
		 *
		 *
		 * @static
		 * @param {string} idUser
		 * @param {string} idCommunity
		 * @param {string} idCodeQr
		 * @param {Function} next
		 *
		 * @memberOf PgqQrCode
		 */
		static scanCodeQr(
			idCodeQr: string,
			next: Function
		): void {
			codeDecrypt(idCodeQr).then(idCodeQrDes => {
				console.log('idCodeQrDes : ',idCodeQrDes);
				scanCodeQr(idCodeQrDes).then(scanResult => {
					console.log('scanResult : ',scanResult);
					 next(null,scanResult);
				}).catch(err => {
					next(err);
				});
			}).catch(err => {
				next(err);
			});
		}

		/**
		 * Servicio que se encarga de validar y generar, codeQr por usuario y comunidad
		 * tomando los tiempos de activación y expiración de la base de datos
		 *
		 * @static
		 *
		 * @memberOf PgqQrCode
		 */
		static generateQrCode(
			idUser: string,
			idUserContact: string,
			idCommunity: string,
			next: Function
		): void {
				//Validar que no hayas codeQr activos del usuario en la comunidad seleccionada
				validUserComunityCodeQr(idUserContact,idCommunity).then(validCodeQr => {
					console.log('validCodeQr : ',validCodeQr);
					if(validCodeQr) {
						//Obtener id members para el envio de codeQr
						getGroupUserDefault(idUser,idCommunity).then(idGroup => {
							getMembers(idGroup,idUserContact).then(objectMembers => {
								//Obtener valores por defecto
									getConfigCommunity(idCommunity).then(configCommunity => {
										console.log('configCommunity : ',configCommunity);
										//Declaración de object de tipo interface
										let objectCodeQr: any = {
											code: '',
											deliveryDate: new Date(),
											timeExpire: configCommunity[0].timeExpDefault,
											created: new Date(),
											status: true,
											fkMembersId: objectMembers[0].id
										};
										console.log('objectCodeQr : ',objectCodeQr);
										//Crear codeQr
										createCodeQr(objectCodeQr).then(createCodeQr => {
												next(null,true);
										}, (err: any) => {
											next(err,false)
										});
									}).catch(err => {
										next(err,false);
									});
							}, (err: any) => {
								next(err,false);
							});
						}, (err: any) => {
							next(err,false);
						});
					}
				}, (err: any) => {
					next(err);
				});
		}

		/**
		 * Servicio que auto genera el codeQR
		 *
		 * @static
		 *
		 * @memberOf PgqQrCode
		 */
		static selfGenerated(
			idUser: string,
			idCommunity: string,
			next: Function
			): void {
				let CodeQR: any = app.models.PGQ_QRCode;
				let deliveryDate: Date = new Date();
				let graceTime: Date = new Date();
				getMembersAutoGenerated(idUser,idCommunity).then(members => {
					console.log('members 2 : ',members);
					if(members.length>0) {
						CodeQR.generateQrCode(members[0].id,
																deliveryDate, graceTime,
						(err, codeQR) => {
							if (err) {next(err,false);}
							console.log('codeQR : ',codeQR);
							if(codeQR) {
								next(null,true);
							} else {
								let error = 'CodeQR not generated.';
								let errQrcode: any = new Error(error);
								errQrcode.statusCode = 400;
								next(errQrcode,false);
							}
						});
					} else {
						next(null,false);
					}
				},(err: any) => {
					next(err, false);
				});
		}
}
module.exports = PgqQrCode;
